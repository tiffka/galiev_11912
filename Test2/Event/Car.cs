﻿using System;

namespace Test_2
{
    public class Car
    {
        public string Name { get; set; }

        private string _priv = "Private field";

        public Car(string name)
        {
            Name = name;
        }

        public event EventHandler<PublishEventArgs> PublishEvent;

        /// <summary>
        /// Выпустить авто
        /// </summary>
        public void Publish(DateTime when, string where, bool isPrivate = false)
        {
            var args = new PublishEventArgs(when, where, isPrivate);

            if (PublishEvent != null)
                PublishEvent(this, args);
        }

        public static void SomeStaticMethod()
        {
            Console.WriteLine("Called static");
        }
    }
}