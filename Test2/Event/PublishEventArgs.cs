﻿using System;

namespace Test_2
{
    public class PublishEventArgs : EventArgs
    {
        private DateTime _when;
        private bool _isPrivate;
        private string _where;

        public PublishEventArgs(DateTime when, string where, bool isPrivate)
        {
            _when = when;
            _where = where;
            _isPrivate = isPrivate;
        }

        public DateTime When
        {
            get { return _when; }
        }

        public string Where
        {
            get { return _where; }
        }

        public bool isPrivate
        {
            get { return _isPrivate; }
        }
    }
}