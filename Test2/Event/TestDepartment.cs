﻿using System;

namespace Test_2
{
    public class TestDepartment
    {
        public void Subscribe(Car car)
        {
            car.PublishEvent += Test;
        }

        public void UnSubscribe(Car car)
        {
            car.PublishEvent -= Test;
        }

        private void Test(object sender, PublishEventArgs e)
        {
            Console.WriteLine("Тестируем авто " +
                              ((Car) sender).Name + " в " +
                              e.Where + " " + e.When.ToShortDateString());
        }
    }
}