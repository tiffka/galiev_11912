﻿using System;

namespace Test_2
{
    public class MarketDepartment
    {
        public void Subscribe(Car car)
        {
            car.PublishEvent += StartCampaign;
        }

        public void UnSubscribe(Car car)
        {
            car.PublishEvent -= StartCampaign;
        }

        private void StartCampaign(object sender, PublishEventArgs e)
        {
            Console.WriteLine("Запускаем продвижение авто " +
                              ((Car) sender).Name + " в " +
                              e.Where + " " + e.When.ToShortDateString());
        }
    }
}