﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Test_2
{
    public class Runner
    {
        public static void LINQ()
        {
            var products = new List<Linq.Product>
            {
                new Linq.Product {Id = 1, Name = "Аквариум 10 литров"},
                new Linq.Product {Id = 2, Name = "Аквариум 20 литров"},
                new Linq.Product {Id = 3, Name = "Аквариум 50 литров"},
                new Linq.Product {Id = 4, Name = "Аквариум 100 литров"},
                new Linq.Product {Id = 5, Name = "Аквариум 200 литров"},
                new Linq.Product {Id = 6, Name = "Фильтр"},
                new Linq.Product {Id = 7, Name = "Термометр"}
            };

            var prices = new List<Linq.Price>
            {
                new Linq.Price {Id = 1, ProductId = 1, Sum = 100, IsActual = false},
                new Linq.Price {Id = 2, ProductId = 1, Sum = 123, IsActual = true},
                new Linq.Price {Id = 3, ProductId = 2, Sum = 234, IsActual = true},
                new Linq.Price {Id = 4, ProductId = 3, Sum = 532, IsActual = true},
                new Linq.Price {Id = 5, ProductId = 4, Sum = 234, IsActual = true},
                new Linq.Price {Id = 6, ProductId = 5, Sum = 534, IsActual = true},
                new Linq.Price {Id = 7, ProductId = 5, Sum = 124, IsActual = false},
                new Linq.Price {Id = 8, ProductId = 6, Sum = 153, IsActual = true},
                new Linq.Price {Id = 9, ProductId = 7, Sum = 157, IsActual = true}
            };


            var invoices = new List<Linq.Invoice>
            {
                new Linq.Invoice
                {
                    Products = new List<Linq.InvoiceProduct>
                    {
                        new Linq.InvoiceProduct {Product = products[1], Quantity = 2},
                        new Linq.InvoiceProduct {Product = products[0], Quantity = 3}
                    }
                },
                new Linq.Invoice
                {
                    Products = new List<Linq.InvoiceProduct>
                    {
                        new Linq.InvoiceProduct {Product = products[5], Quantity = 1},
                        new Linq.InvoiceProduct {Product = products[6], Quantity = 2}
                    }
                },
                new Linq.Invoice
                {
                    Products = new List<Linq.InvoiceProduct>
                    {
                        new Linq.InvoiceProduct {Product = products[6], Quantity = 1},
                        new Linq.InvoiceProduct {Product = products[4], Quantity = 1},
                        new Linq.InvoiceProduct {Product = products[5], Quantity = 2}
                    }
                },
                new Linq.Invoice
                {
                    Products = new List<Linq.InvoiceProduct>
                    {
                        new Linq.InvoiceProduct {Product = products[6], Quantity = 1},
                        new Linq.InvoiceProduct {Product = products[2], Quantity = 1},
                        new Linq.InvoiceProduct {Product = products[5], Quantity = 1}
                    }
                }
            };


            var res = invoices[0].Products.Join(
                    prices,
                    prod => prod.Product.Id,
                    price => price.ProductId,
                    (prod, price) => new {Quantity = prod.Quantity, Product = prod.Product, Price = price}
                ).Where(x => x.Price.IsActual == true)
                .OrderBy(x => x.Product.Name);


            Console.WriteLine("Наименование услуги    Сумма    Итого");
            foreach (var invoiceProducts in res)
            {
                Console.WriteLine(
                    $"{invoiceProducts.Product.Name}    {invoiceProducts.Price.Sum}    {invoiceProducts.Price.Sum * invoiceProducts.Quantity} ");
            }


            var res1 = products.Join(prices,
                    prod => prod.Id,
                    price => price.ProductId,
                    (prod, price) => new {Product = prod, Price = price})
                .GroupBy(x => x.Product.Id);

            Console.WriteLine("Средние цены: ");
            foreach (var item in res1)
            {
                Console.WriteLine(item.First().Product.Name + " " + item.Average(x => x.Price.Sum));
            }


            var sales = new List<Linq.Sale>
            {
                new Linq.Sale
                {
                    Products = new List<Linq.ProductSale>
                    {
                        new Linq.ProductSale {Products = new List<Linq.Product> {products[4]}, Quantity = 1},
                        new Linq.ProductSale {Products = new List<Linq.Product> {products[5]}, Quantity = 2},
                    },
                    SaleSum = 15
                },
                new Linq.Sale
                {
                    Products = new List<Linq.ProductSale>
                    {
                        new Linq.ProductSale {Products = new List<Linq.Product> {products[3]}, Quantity = 1},
                        new Linq.ProductSale {Products = new List<Linq.Product> {products[5]}, Quantity = 2},
                    },
                    SaleSum = 10
                },
                new Linq.Sale
                {
                    Products = new List<Linq.ProductSale>
                    {
                        new Linq.ProductSale
                            {Products = new List<Linq.Product> {products[0], products[1], products[2]}, Quantity = 1},
                        new Linq.ProductSale {Products = new List<Linq.Product> {products[5]}, Quantity = 1},
                    },
                    SaleSum = 5
                },
            };


            /*Задания              *
             * 1) создать список счетов (один счет содержит несколько пар цена-количество)
             * например, один счет - это аквариум на 200 литров, два фильтра и термометр
             * 2) вывести счет для покупателя с колонками "Наименование услуги, сумма, итого"
             * в порядке сортировки по наименованию товара в лексикографическом порядке
             *  - 1 балл
             * 3) Вывести среднюю цену для каждого продукта с учетом неактуальных значений
             * - 1 балл
             * 4) создать список акций (код продукта, скидка):
             * аквариум на 200 литров + 2 фильтра - скидка 15%
             * аквариум 100 литров + 2 фильтра - 10% скидка
             * любой другой аквариум + фильтр - 5% скидка
             * 5) создать список перечень всех названий товаров в группе акции +
             * цена до скидки + цена с учетом скидки  - 1 балл
             *
             * Для тех, кто выбрал вариант посложнее: написать функцию подсчета
             * суммы покупки (выявлять, есть ли в наборе продуктов акционные комплекты,
             * при их наличии делать скидку),
             * выводить итого без учета скидки и со скидкой, отельно сумму скидки
             * - это + 2 балла
             * использовать Linq операции со множествами
             */


            /*
             * Приходится создавать три уровня вложенности, чтобы создать группу скидок.
             * Можно было сделать GroupSale -> Sales -> Products, но это тоже три уровня вложенности
             * В SQL я бы сделал множественный JOIN
             */
            foreach (var sale in sales)
            {
                Console.WriteLine($"Sale for {sale.SaleSum}%");
                foreach (var productSale in sale.Products)
                {
                    var data = prices
                        .Join(productSale.Products,
                            pr => pr.ProductId,
                            prod => prod.Id,
                            (pr, prod) => new {Product = prod, Price = pr}
                        ).Where(x => x.Price.IsActual == true);

                    List<string> productStrings = new List<string>();

                    foreach (var product in data)
                    {
                        var newPrice = product.Price.Sum * (1 - Convert.ToDecimal(sale.SaleSum) / 100);

                        productStrings.Add(
                            $"Product: {product.Product.Name}, Previous price: {product.Price.Sum}, New price: {newPrice}, Min qty: {productSale.Quantity}");
                    }

                    Console.WriteLine(String.Join(" OR \n", productStrings));
                }
            }

            /*последнее задание не доделал*/
            /*var invoice = invoices[2];

            var res34 = sales.Select(
                x => x.Products.Select(
                    prodSale => prodSale.Products.Where(
                        prod => invoice.Products.Select(prod2=>prod2.Product.Id).Contains(prod.Id)
                    )
                )
            );
            return;*/
        }

        public static void TreeRunner()
        {
            Tree tree = new Tree
            {
                Node = new Tree.TreeNode
                {
                    Value = 5,
                    Children = new Tree.TreeNode[]
                    {
                        new Tree.TreeNode {Value = 4},
                        new Tree.TreeNode
                        {
                            Value = 3,
                            Children = new Tree.TreeNode[]
                            {
                                new Tree.TreeNode
                                {
                                    Value = 2,
                                },
                                new Tree.TreeNode {Value = 1}
                            },
                        }
                    },
                }
            };

            tree.CLR(tree.Node, WriteQuote);
        }


        public static void WriteQuote(string s)
        {
            Console.WriteLine($"\"{s}\" ");
        }

        public static void Event()
        {
            var carToyota = new Car("Toyota");
            var td = new TestDepartment();
            var md = new MarketDepartment();

            td.Subscribe(carToyota);
            md.Subscribe(carToyota);

            carToyota.Publish(new DateTime(2020, 5, 15), "Moscow");
        }
    }
}