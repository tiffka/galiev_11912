﻿using System;

namespace Test_2
{
    public class Tree
    {
        public delegate void Write(string s);
        /*сделать обход дерева (дерево общего вида) способом
        * 3) в глубину префиксный
        * Передать вторым параметром делегат, на место которого можно будет
        * передавать функцию вывода на консоль с различными видами форматирования
        * Функция вывода:
        * 1) каждый элемент оборачивать в кавычки
        */

        public class TreeNode
        {
            public int Value;
            public TreeNode[] Children;
        }

        public TreeNode Node;

        public void CLR(TreeNode node, Write write)
        {
            if (node != null)
            {
                write(node.Value.ToString());
                if (node.Children != null)
                    foreach (var child in node.Children)
                    {
                        CLR(child, write);
                    }
            }
        }
    }
}