﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Test_2
{
    public class LINQ2
    {
        public static void Run()
        {
            var products = new List<Product>
            {
                new Product {Id = 1, Name = "Аквариум 10 литров"},
                new Product {Id = 2, Name = "Аквариум 20 литров"},
                new Product {Id = 3, Name = "Аквариум 50 литров"},
                new Product {Id = 4, Name = "Аквариум 100 литров"},
                new Product {Id = 5, Name = "Аквариум 200 литров"},
                new Product {Id = 6, Name = "Фильтр"},
                new Product {Id = 7, Name = "Термометр"}
            };

            var prices = new List<Price>
            {
                new Price {Id = 1, ProductId = 1, Sum = 100, IsActual = false},
                new Price {Id = 2, ProductId = 1, Sum = 123, IsActual = true},
                new Price {Id = 3, ProductId = 2, Sum = 234, IsActual = true},
                new Price {Id = 4, ProductId = 3, Sum = 532, IsActual = true},
                new Price {Id = 5, ProductId = 4, Sum = 234, IsActual = true},
                new Price {Id = 6, ProductId = 5, Sum = 534, IsActual = true},
                new Price {Id = 7, ProductId = 5, Sum = 124, IsActual = false},
                new Price {Id = 8, ProductId = 6, Sum = 153, IsActual = true},
                new Price {Id = 9, ProductId = 7, Sum = 157, IsActual = true}
            };


            var invoices = new List<Invoice>
            {
                new Invoice
                {
                    ID = 1
                },
                new Invoice
                {
                    ID = 2
                },
                new Invoice
                {
                    ID = 3
                }
            };


            var invoiceProducts = new List<InvoiceProduct>
            {
                new InvoiceProduct {ProductId = 2, Quantity = 2, InvoiceId = 1},
                new InvoiceProduct {ProductId = 1, Quantity = 3, InvoiceId = 1},

                new InvoiceProduct {ProductId = 7, Quantity = 1, InvoiceId = 2},
                new InvoiceProduct {ProductId = 4, Quantity = 1, InvoiceId = 2},
                new InvoiceProduct {ProductId = 5, Quantity = 1, InvoiceId = 2},
                new InvoiceProduct {ProductId = 6, Quantity = 2, InvoiceId = 2},

                new InvoiceProduct {ProductId = 7, Quantity = 1, InvoiceId = 3},
                new InvoiceProduct {ProductId = 3, Quantity = 1, InvoiceId = 3},
                new InvoiceProduct {ProductId = 6, Quantity = 1, InvoiceId = 3}
            };


            var sales = new List<Sale>
            {
                new Sale
                {
                    Id = 1,
                    SaleSum = 15
                },
                new Sale
                {
                    Id = 2,
                    SaleSum = 10
                },
                new Sale
                {
                    Id = 3,
                    SaleSum = 5
                },
                new Sale
                {
                    Id = 4,
                    SaleSum = 5
                },
                new Sale
                {
                    Id = 5,
                    SaleSum = 5
                }
            };
            var saleProducts = new List<ProductSale>
            {
                new ProductSale {SaleID = 1, Quantity = 1, ProductID = 5},
                new ProductSale {SaleID = 1, Quantity = 2, ProductID = 6},

                new ProductSale {SaleID = 2, Quantity = 1, ProductID = 4},
                new ProductSale {SaleID = 2, Quantity = 2, ProductID = 6},

                new ProductSale {SaleID = 3, Quantity = 1, ProductID = 1},
                new ProductSale {SaleID = 3, Quantity = 1, ProductID = 6},

                new ProductSale {SaleID = 4, Quantity = 1, ProductID = 2},
                new ProductSale {SaleID = 4, Quantity = 1, ProductID = 6},

                new ProductSale {SaleID = 5, Quantity = 1, ProductID = 3},
                new ProductSale {SaleID = 5, Quantity = 1, ProductID = 6}
            };

            foreach (var invoice in invoices)
            {
                var currentInvoiceProducts = invoiceProducts.Where(x => x.InvoiceId == invoice.ID);
                var countMinus = new List<CountMinus>();

                var salesGroup = currentInvoiceProducts.Join(
                        saleProducts,
                        prod => prod.ProductId,
                        sale => sale.ProductID,
                        (prod, sale) => new {InvoiceProduct = prod, ProductSale = sale}
                    ).Join(
                        sales,
                        arg => arg.ProductSale.SaleID,
                        sale => sale.Id,
                        (arg, sale) =>
                            new {arg.InvoiceProduct, arg.ProductSale, Sale = sale}
                    )
                    .Where(arg => arg.InvoiceProduct.Quantity >= arg.ProductSale.Quantity)
                    .OrderByDescending(arg => arg.Sale.SaleSum)
                    .GroupBy(arg => arg.ProductSale.SaleID)
                    .Select(arg => new {Results = arg, Count = arg.Count()})
                    .Where(x => x.Count == saleProducts.Count(a => a.SaleID == x.Results.Key));


                if (!salesGroup.Any()) continue;
                Console.WriteLine("Акционные комплекты:");
                decimal saleSum = 0;
                var productStrings = new List<string>();
                foreach (var item in salesGroup)
                {
                    decimal complectSaleSum = 0;
                    var minComplectCount = int.MaxValue;
                    foreach (var arg in item.Results)
                    {
                        if (arg.InvoiceProduct.Quantity - countMinus
                            .Where(x => x.ProductId == arg.InvoiceProduct.ProductId).Sum(x => x.Count) < arg.ProductSale.Quantity)
                        {
                            minComplectCount = 0;
                            continue;
                        }

                        var product = products.First(x => x.Id == arg.InvoiceProduct.ProductId);
                        var price = prices.First(x => x.ProductId == product.Id && x.IsActual);
                        var newPrice = arg.ProductSale.Quantity * price.Sum *
                                       (1 - Convert.ToDecimal(arg.Sale.SaleSum) / 100);


                        complectSaleSum += price.Sum * arg.ProductSale.Quantity - newPrice;
                        var complectCount = arg.InvoiceProduct.Quantity / arg.ProductSale.Quantity;

                        countMinus.Add(new CountMinus
                        {
                            ProductId = arg.InvoiceProduct.ProductId, Count = complectCount * arg.ProductSale.Quantity
                        });

                        if (complectCount < minComplectCount) minComplectCount = complectCount;

                        if (minComplectCount == 0) continue;


                        string str = product.Name + " " + arg.InvoiceProduct.Quantity + " шт" +
                                     " старая цена: " + price.Sum * arg.ProductSale.Quantity +
                                     " новая цена: " + newPrice;

                        productStrings.Add(str);
                    }

                    if(minComplectCount > 0)
                        Console.WriteLine(String.Join("\n", productStrings));

                    complectSaleSum *= minComplectCount;
                    saleSum += complectSaleSum;
                }

                var sumFull = currentInvoiceProducts
                    .Join(prices, prod => prod.ProductId, price => price.ProductId,
                        (prod, price) => new {price.Sum, price.IsActual, Qty = prod.Quantity})
                    .Where(x => x.IsActual).Sum(x => x.Sum * x.Qty);
                var sumFullSale = sumFull - saleSum;
                Console.WriteLine("Товары:");
                var currProductsFull = currentInvoiceProducts
                    .Join(products, invproduct => invproduct.ProductId,
                        product => product.Id,
                        (invproduct, product) => new {Product = product, InvoiceProduct = invproduct})
                    .Join(prices, invproduct => invproduct.Product.Id,
                        price => price.ProductId,
                        (invproduct, price) => new {invproduct.Product, invproduct.InvoiceProduct, Price = price})
                    .Where(x => x.Price.IsActual);

                foreach (var ctx in currProductsFull)
                    Console.WriteLine(ctx.Product.Name + " " + ctx.InvoiceProduct.Quantity + " шт. " + ctx.Price.Sum +
                                      " руб");

                Console.WriteLine("Цена без скидок: " + sumFull);
                Console.WriteLine("Цена со скидками: " + sumFullSale);
            }
        }

        public class CountMinus
        {
            public int ProductId { get; set; }
            public int Count { get; set; }
        }

        public class Product
        {
            public int Id { get; set; }
            public string Name { get; set; }
        }

        public class Price
        {
            public int Id { get; set; }
            public int ProductId { get; set; }
            public decimal Sum { get; set; }
            public bool IsActual { get; set; }
        }

        public class Invoice
        {
            public int ID { get; set; }
        }

        public class InvoiceProduct
        {
            public int ProductId { get; set; }
            public int InvoiceId { get; set; }
            public int Quantity { get; set; }
        }

        public class Sale
        {
            public int Id { get; set; }
            public int SaleSum { get; set; }
        }

        public class ProductSale
        {
            public int ProductID { get; set; }
            public int Quantity { get; set; }
            public int SaleID { get; set; }
        }
    }
}