﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Test_2
{
    /// <summary>
    /// просто на 3 балла, сложный на 5 баллов
    /// </summary>
    public class Linq
    {
        public class Product
        {
            public int Id { get; set; }
            public string Name { get; set; }
        }

        public class Price
        {
            public int Id { get; set; }
            public int ProductId { get; set; }
            public decimal Sum { get; set; }
            public bool IsActual { get; set; }
        }

        public class Invoice
        {
            public List<InvoiceProduct> Products { get; set; }
        }

        public class InvoiceProduct
        {
            public Product Product { get; set; }
            public int Quantity { get; set; }
        }

        public class Sale
        {
            public int SaleSum { get; set; }
            public List<ProductSale> Products { get; set; }
        }

        public class ProductSale
        {
            public List<Product> Products { get; set; }
            public int Quantity { get; set; }
        }
    }
}