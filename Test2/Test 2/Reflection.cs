﻿using System;
using System.Reflection;

namespace Test_2
{
    public class Reflection
    {
        /* создать в классе-источнике события
     * статический метод и вызовите его,
     * непубличное поле, которому зададите значение и получите,
     * публичное свойство, которому зададите значение и получите его
     * - 1 балл
     *
     * Создайте рефлексией классы-подписчики и класс-источник, подпишитесь на событие,
     * вызовите событие
     * - 2 балла
     */

        public static void Run()
        {
            var a = Assembly.LoadFrom(@"C:\Users\iljas\YandexDisk\Work\Edu\C#\Test 2\Event\bin\Debug\Event.dll");

            Type classType = a.GetType("Test_2.Car");
            var name = "Toyota";
            object classCar = Activator.CreateInstance(classType, new object[] {name});
            MethodInfo method = classType.GetMethod("SomeStaticMethod");

            object result = method.Invoke(classCar, new object[] { });

            PropertyInfo pi = classType.GetProperty("Name");
            var result2 = pi.GetValue(classCar);
            Console.WriteLine(result2);
            FieldInfo fi = classType.GetField("_priv", BindingFlags.NonPublic | BindingFlags.Instance);
            var result3 = fi.GetValue(classCar);
            Console.WriteLine(result3);


            Type classTypeMD = a.GetType("Test_2.MarketDepartment");
            object classMD = Activator.CreateInstance(classTypeMD, new object[] { });

            Type classTypeTD = a.GetType("Test_2.TestDepartment");
            object classTD = Activator.CreateInstance(classTypeTD, new object[] { });


            MethodInfo regMD = classTypeMD.GetMethod("Subscribe");
            regMD.Invoke(classMD, new object[] {classCar});

            MethodInfo regTD = classTypeTD.GetMethod("Subscribe");
            regTD.Invoke(classTD, new object[] {classCar});


            MethodInfo pub = classType.GetMethod("Publish");
            pub.Invoke(classCar, new object[] {new DateTime(2020, 5, 10), "Tokyo", false});
        }
    }
}