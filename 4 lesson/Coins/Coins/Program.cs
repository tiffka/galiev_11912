﻿using System;
using System.Collections.Generic;
using System.Linq;

/*
 * простите что всё в одном файле
 */
namespace Coins
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            List<string> variants = new List<string>();
            int[] coins = new int[] {1, 2, 5};

            Console.Write("Enter sum: ");
            Int32.TryParse(Console.ReadLine(), out int sum);

            F(sum, variants, new List<int>(), coins);

            Console.WriteLine("Possible variants:");
            foreach (var item in variants)
            {
                Console.WriteLine(item);
            }
        }

        /// <summary>
        /// Алгоритм работает в виде дерева
        ///        -5 -> меньше нуля
        ///        -2 -> 2 -> 1+1
        /// 4 ->           -> 2
        ///        -1 -> 3 -> -1 -> 2 -> 1+1
        ///                     -> 2
        ///                -> -2 -> 1
        /// currentVariant хранит слагаемые для конкретной рассматриваемой веточки дерева
        /// variants хранит все возможные варианты
        /// </summary>
        static void F(int sum, List<string> variants, List<int> currentVariant, int[] coins)
        {
            if (sum < 0) return;

            if (sum == 0)
            {
                currentVariant.Sort();
                var str = String.Join("+", currentVariant.ToArray());
                if (!variants.Contains(str))
                {
                    variants.Add(str);
                }
                return;
            }

            foreach (var coin in coins)
            {
                if (sum >= coin)
                {
                    var newVariant = currentVariant.ToList();
                    newVariant.Add(coin);

                    F(sum - coin, variants, newVariant, coins);
                }
            }
        }
    }
}