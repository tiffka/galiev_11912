﻿/**
* @author Ilyas Galiev
* 11-912
* C1
*/


using System;

namespace ConsoleApplication1
{
    class Program
    {
        public static void Main()
        {
            Console.Write("Enter x: ");
            double x = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Math: sqrt(1+x) = " + Math.Sqrt(1 + x));

            double eps = 1e-7, y1, y2 = 0.0, d = 1.0;
            double nFact = 1;
            int i = 0, k = 1;
            do
            {
                y1 = y2;
                if (i > 0)
                {
                    nFact *= i;
                    d *= x * 2 * i * (2 * i - 1);
                    d /= 4;
                    k *= -1;
                }

                y2 += k * d / ((1 - 2 * i) * nFact * nFact);
                i++;
            } while (Math.Abs(y2 - y1) > eps);

            Console.WriteLine("sqrt(1+x) = " + y2);
        }
    }
}