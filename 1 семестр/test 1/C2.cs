﻿/**
* @author Ilyas Galiev
* 11-912
* C2
*/


using System;

namespace ConsoleApplication1
{
    class Program
    {
        public static void Main()
        {
            for (int i = 1000; i <= 9999; i++)
            {
                int tmp = i;
                int[] a = new int[4]; // создаем массив из цифр числа

                int j = 0;
                while (tmp > 0)
                {
                    a[j] = tmp % 10;
                    j++;
                    tmp /= 10;
                }
                bool flag = true;

                // проверяем массив на уникальность
                for (int k = 0; k < 3; k++)
                for (int l = k+1; l < 4; l++)
                {
                    if (a[k] == a[l]) { flag = false;}
                }
                
                if(flag) Console.WriteLine(i);
            }
        }
    }
}