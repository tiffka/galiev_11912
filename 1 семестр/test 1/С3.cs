﻿/**
* @author Ilyas Galiev
* 11-912
* C3
*/


using System;

namespace ConsoleApplication1
{
    class Program
    {
        public static void Main()
        {
            Console.Write("Enter a: ");
            int a = Convert.ToInt32(Console.ReadLine());
            Console.Write("Enter b: ");
            int b = Convert.ToInt32(Console.ReadLine());

            for (int i = a; i < b; i++)
            {
                int s1 = 0;
                for (int j = 1; j < i; j++) // считаем делители первого числа
                    if (i % j == 0)
                        s1 += j;

                // ищем дружественное число
                for (int j = i + 1; j <= b; j++)
                {
                    // если сумма делителей первого числа равна второму числу, то проверяем равенство делителей второго числа первому
                    if (j == s1)
                    {
                        int s2 = 0;
                        for (int k = 1; k < j; k++)
                            if (j % k == 0)
                                s2 += k;
                        if (s2==i) Console.WriteLine($"{i} {j}");
                    }
                }
            }
        }
    }
}