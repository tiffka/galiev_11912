﻿/**
* @author Ilyas Galiev
* 11-912
* Task 22
*/


using System;

namespace ConsoleApplication1
{
    class Program
    {
        public static void Main()
        {
            Console.Write("enter array size: ");
            int n = Convert.ToInt32(Console.ReadLine());
            int[] a = new int[n];

            Console.Write("enter array elements: ");
            for (int i = 0; i < n; i++)
                a[i] = Convert.ToInt32(Console.ReadLine());

            int min = a[0], max = a[0], minIndex = 0, maxIndex = 0;

            for (int i = 1; i < n; i++)
            {
                if (a[i] > max)
                {
                    max = a[i];
                    maxIndex = i;
                }

                if (a[i] < min)
                {
                    min = a[i];
                    minIndex = i;
                }
            }

            int k = Math.Abs(maxIndex - minIndex) - 1;
            int[] b = new int[n - k];

            int start = maxIndex > minIndex ? minIndex : maxIndex;
            int end = maxIndex < minIndex ? minIndex : maxIndex;
            int j = 0;

            Console.Write("Result: ");
            for (int i = 0; i < n; i++)
            {
                if (i <= start || i >= end)
                {
                    b[j] = a[i];
                    Console.Write($"{b[j]} ");
                    j++;
                }
            }
        }
    }
}