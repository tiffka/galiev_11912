﻿﻿/**
* @author Ilyas Galiev
* 11-912
* Task 24
*/


using System;

namespace ConsoleApplication1
{
    class Program
    {
        public static void Main()
        {
            Console.WriteLine("enter array size: ");
            int n = Convert.ToInt32(Console.ReadLine());
            int[] a = new int[n];


            Console.WriteLine("enter number: ");
            for (int i = n - 1; i >= 0; i--)
            {
                a[i] = Convert.ToInt32(Console.ReadLine());
            }

            Console.WriteLine("enter array 2 size: ");
            int n2 = Convert.ToInt32(Console.ReadLine());
            int[] a2 = new int[n];//new int[n2]


            Console.WriteLine("enter number 2: ");
            for (int i = n2 - 1; i >= 0; i--)
            {
                a2[i] = Convert.ToInt32(Console.ReadLine());
            }


            int[] res = new int[n + n2];

            for (int i = 0; i < n + n2 - 1; i++)
            {
                res[i] = 0;
            }

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n2; j++)
                {
                    int tmp = a[i] * a2[j] + res[i + j];
                    res[i + j] = tmp;
                }
            }

            for (int i = 0; i < n + n2 - 2; i++)
            {
                if (res[i] > 10)
                {
                    int t = res[i] / 10;
                    res[i] = res[i] % 10;
                    res[i + 1] += t;
                }
            }

            for (int i = n + n2 - 1; i >= 0; i--)
            {
                if (i == n + n2 - 1 && res[i] == 0) continue;
                Console.Write($"{res[i]}");
            }
        }
    }
}