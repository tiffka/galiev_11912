﻿/**
* @author Ilyas Galiev
* 11-912
* Task 23
*/


using System;

namespace ConsoleApplication1
{
    class Program
    {
        public static void Main()
        {
            Console.WriteLine("enter array size: ");
            int n = Convert.ToInt32(Console.ReadLine());
            int[] a = new int[n];
            
            
            Console.WriteLine("enter number: ");
            for (int i = 0; i < n; i++)
            {
                a[i] = Convert.ToInt32(Console.ReadLine());
            }
            
            
            Console.WriteLine("enter digit: ");
            int k = Convert.ToInt32(Console.ReadLine());
            int t=0;
            
            for (int i = n-1; i >= 0; i--)
            {
                int tmp = a[i];
                a[i] = (a[i] * k + t) % 10;
                t = (tmp * k + t) / 10;
            }

            if (t != 0) a[0] = t * 10 + a[0];

            for (int i = 0; i < n; i++)
            {
                Console.Write($"{a[i]}");
            }
        }
    }
}