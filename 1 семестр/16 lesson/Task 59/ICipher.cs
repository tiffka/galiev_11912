﻿namespace ConsoleApplication3
{
    public interface ICipher
    {
        string encode(string s);
        string decode(string s);
    }
}