﻿using System;

namespace ConsoleApplication3
{
    public class Program
    {
        static void Main()
        {
            ACipher aCipher = new ACipher();
            string s = "abcd";
            string s2 = aCipher.encode(s);
            string s3 = aCipher.decode(s2);

            Console.WriteLine(s2);
            Console.WriteLine(s3);

            BCipher bCipher = new BCipher();
            s = "AbCd";
            s2 = bCipher.encode(s);
            s3 = bCipher.decode(s2);
            
            Console.WriteLine(s2);
            Console.WriteLine(s3);
        }
    }
}