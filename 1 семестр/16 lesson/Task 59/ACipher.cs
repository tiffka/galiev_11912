﻿using System;

namespace ConsoleApplication3
{
    public class ACipher : ICipher
    {
        public string encode(string s)
        {
            string s2 = "";
            for (int i = 0; i < s.Length; i++)
            {
                s2 += Convert.ToChar(s[i] + 1);
            }

            return s2;
        }

        public string decode(string s)
        {
            string s2 = "";
            for (int i = 0; i < s.Length; i++)
            {
                s2 += Convert.ToChar(s[i] - 1);
            }

            return s2;
        }
    }
}