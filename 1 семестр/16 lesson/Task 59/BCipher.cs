﻿using System;

namespace ConsoleApplication3
{
    public class BCipher : ICipher
    {
        public string encode(string s)
        {
            string s2 = "";

            for (int i = 0; i < s.Length; i++)
            {
                if (s[i] >= 'a' && s[i] <= 'z')
                    s2 += Convert.ToChar(122 - s[i] + 97);
                if (s[i] >= 'A' && s[i] <= 'Z')
                    s2 += Convert.ToChar(90 - s[i] + 65);
            }

            return s2;
        }

        public string decode(string s)
        {
            return encode(s);
        }
    }
}