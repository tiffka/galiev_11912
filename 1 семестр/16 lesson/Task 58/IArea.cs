﻿namespace ConsoleApplication3
{
    public interface IArea
    {
        double Area();
    }
}