﻿/**
* @author Ilyas Galiev
* 11-912
* Task 58
*/

namespace ConsoleApplication3
{
    public abstract class Figure : IColor
    {
        private bool visible;
        private string color;

        public bool Visible
        {
            get => visible;
            set => visible = value;
        }

        public string Color
        {
            get => color;
            set => color = value;
        }

        public string Visibility()
        {
            return Visible ? "visible" : "unvisible";
        }

        public override string ToString()
        {
            return $"Visibility: {Visibility()}, color: {Color}";
        }

        public virtual void ChangeColor(string color)
        {
            this.Color = color;
        }
    }
}