﻿namespace ConsoleApplication3
{
    public interface IMovable
    {
        void MoveX(double x);
        void MoveY(double y);
    }
}