﻿/**
* @author Ilyas Galiev
* 11-912
* Task 58 
*/

using System;

namespace ConsoleApplication3
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Point p = new Point();
            Console.WriteLine(p);
            p.ChangeColor("white");
            Console.WriteLine(p);
            p.MoveX(2);
            Console.WriteLine(p);

            Circle c = new Circle(2.5);
            Console.WriteLine(c);
            Console.WriteLine(c.Area());

            Rectangle rect = new Rectangle(3, 4, 5);
            Console.WriteLine(rect);
            Console.WriteLine(rect.Area());
        }
    }
}