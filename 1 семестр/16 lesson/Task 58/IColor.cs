﻿namespace ConsoleApplication3
{
    public interface IColor
    {
        void ChangeColor(string color);
    }
}