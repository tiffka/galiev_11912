﻿/**
* @author Ilyas Galiev
* 11-912
* Task 33
*/
using System;
namespace ConsoleApp2
{
    class Program
    {
        static void Main()
        {
            Int32.TryParse(Console.ReadLine(), out int n);
            Console.WriteLine(Fact(n));
        }

        static int Fact(int n)
        {
            if (n == 1) return n;
            return n * Fact(n - 1);
        }
    }
}