﻿/**
* @author Ilyas Galiev
* 11-912
* Task 35
*/

using System;

namespace ConsoleApp2
{
    class Program
    {
        static void Main()
        {
            Int32.TryParse(Console.ReadLine(), out int n);
            Console.WriteLine(Fib(n, 1, 1));
        }

        static int Fib(int n, int n1, int n2)
        {
            if (n < 0) return -1;
            if (n == 0) return 0;
            if (n <= 2) return 1;
            if (n <= 3) return n1 + n2;
            
            return Fib(n - 1, n1 + n2, n1);
        }
    }
}