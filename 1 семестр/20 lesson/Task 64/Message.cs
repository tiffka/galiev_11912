﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace ConsoleApplication3
{
    public enum MessageStatus
    {
        NotRead,
        Read
    }

    public class Message
    {
        private User sender_id, receiver_id;
        private MessageStatus status;
        private string text;
        private DateTime timesent;

        public User SenderId
        {
            get => sender_id;
            set => sender_id = value;
        }

        public User ReceiverId
        {
            get => receiver_id;
            set => receiver_id = value;
        }

        public MessageStatus Status
        {
            get => status;
            set => status = value;
        }

        public string Text
        {
            get => text;
            set => text = value;
        }

        public DateTime Timesent
        {
            get => timesent;
            set => timesent = value;
        }

        public Message(User receiverId, User senderId, int status, string timesent, string text)
        {
            ReceiverId = receiverId;
            SenderId = senderId;
            Status = status == 1 ? MessageStatus.Read : MessageStatus.NotRead;
            Text = text;
            Timesent = DateTime.Parse(timesent);
        }
    } 

    public class MessageComparer : IComparer<Message>
    {
        public int Compare(Message x, Message y)
        {
            if (x.Timesent > y.Timesent) return 1;
            else if (x.Timesent == y.Timesent) return 0;
            else return -1;
        }
    }
}