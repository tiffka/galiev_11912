﻿public enum Genders
{
    Male = 1,
    Female
}

namespace ConsoleApplication3
{
    public class User
    {
        private int id;
        private Genders gender;
        private string username, email, password;

        public int Id
        {
            get => id;
            set => id = value;
        }

        public Genders Gender
        {
            get => gender;
            set => gender = value;
        }

        public string Username
        {
            get => username;
            set => username = value;
        }

        public string Password
        {
            get => password;
            set => password = value;
        }

        public string Email
        {
            get => email;
            set => email = value;
        }

        public User(int id, string username, string email, string password, string gender)
        {
            Id = id;
            Username = username;
            Email = email;
            Password = password;
            Gender = gender == "male" ? Genders.Male : Genders.Female;
        }
    }
}