﻿namespace ConsoleApplication3
{
    public class Subscription
    {
        private User subscriber_id, subscription_id;

        public User SubscriberId
        {
            get => subscriber_id;
            set => subscriber_id = value;
        }

        public User SubscriptionId
        {
            get => subscription_id;
            set => subscription_id = value;
        }

        public Subscription(User subscriberId, User subscriptionId)
        {
            SubscriberId = subscriberId;
            SubscriptionId = subscriptionId;
        }
    }
}