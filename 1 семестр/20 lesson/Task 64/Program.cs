﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ConsoleApplication3
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            string s;
            StreamReader strd;
            strd = new StreamReader("users.txt");

            List<User> users = new List<User>();

            while ((s = strd.ReadLine()) != null)
            {
                string[] fields = s.Split(';');

                User user = new User(Convert.ToInt32(fields[0]), fields[1], fields[4], fields[2], fields[3]);
                users.Add(user);
            }

            strd = new StreamReader("messages.txt");

            List<Message> messages = new List<Message>();

            while ((s = strd.ReadLine()) != null)
            {
                string[] fields = s.Split(';');

                User sender = users.Find(x => x.Id == Convert.ToInt32(fields[0]));
                User receiver = users.Find(x => x.Id == Convert.ToInt32(fields[1]));
                Message message = new Message(receiver, sender,
                    Convert.ToInt32(fields[4]), fields[2], fields[3]);

                messages.Add(message);
            }

            strd = new StreamReader("subscriptions.txt");

            List<Subscription> subscriptions = new List<Subscription>();

            while ((s = strd.ReadLine()) != null)
            {
                string[] fields = s.Split(';');


                User subscriber = users.Find(x => x.Id == Convert.ToInt32(fields[0]));
                User subscriptionUser = users.Find(x => x.Id == Convert.ToInt32(fields[1]));

                Subscription subscription =
                    new Subscription(subscriber, subscriptionUser);
                subscriptions.Add(subscription);
            }

            /* ВЫВОД ПЕРЕПИСКИ */

            Console.WriteLine("Enter first user name");
            string senderName = Console.ReadLine();
            Console.WriteLine("Enter second user name");
            string receiverName = Console.ReadLine();

            User user1 = users.Find(x => x.Username == senderName);
            User user2 = users.Find(x => x.Username == receiverName);

            if (user1 != null && user2 != null)
            {
                List<Message> list = messages.FindAll(x => (x.SenderId.Id == user1.Id && x.ReceiverId.Id == user2.Id));
                list.AddRange(messages.FindAll(x => x.SenderId.Id == user2.Id && x.ReceiverId.Id == user1.Id));
                list.Sort(new MessageComparer());


                for (int i = 0; i < list.Count; i++)
                {
                    Console.WriteLine($"{list[i].SenderId.Username} {list[i].Timesent.ToString()}");
                    Console.WriteLine($"{list[i].Text} ({list[i].Status})");
                    Console.WriteLine();
                }
            }


            /* ВЫВОД ПОДПИСЧИКОВ */

            for (int i = 0; i < subscriptions.Count; i++)
                Console.WriteLine(
                    $"{subscriptions[i].SubscriberId.Username} subscribed on {subscriptions[i].SubscriptionId.Username}");


            /* СТАТИСТИКА ПО СООБЩЕНИЯМ */

            int male = 0;
            int female = 0;

            for (int i = 0; i < messages.Count; i++)
                if (messages[i].SenderId.Gender == Genders.Male) male++;
                else female++;

            Console.WriteLine(male > female ? "Male sent more messages" : "Female sent more messages");

            int fromMaleNotRead = 0;
            int fromMaleRead = 0;
            int fromMaleCount = 0;

            var fromMale = messages.FindAll(x =>
                x.SenderId.Gender == Genders.Male && x.ReceiverId.Gender == Genders.Female);

            foreach (var msg in fromMale)
            {
                if (msg.Status == MessageStatus.Read) fromMaleRead++;
                else fromMaleNotRead++;

                fromMaleCount++;
            }

            int fromFeMaleNotRead = 0;
            int fromFeMaleRead = 0;
            int fromFeMaleCount = 0;

            var fromFeMale = messages.FindAll(x =>
                x.SenderId.Gender == Genders.Female && x.ReceiverId.Gender == Genders.Male);

            foreach (var msg in fromFeMale)
            {
                if (msg.Status == MessageStatus.Read) fromFeMaleRead++;
                else fromFeMaleNotRead++;

                fromFeMaleCount++;
            }

            Console.WriteLine(
                $"From male to female: read - {(double) fromMaleRead / fromMaleCount * 100}%, not read - {(double) fromMaleNotRead / fromMaleCount * 100}%");
            Console.WriteLine(
                $"From female to male: read - {(double) fromFeMaleRead / fromFeMaleCount * 100}%, not read - {(double) fromFeMaleNotRead / fromFeMaleCount * 100}%");
        }
    }
}