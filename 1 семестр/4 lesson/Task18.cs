﻿/**
* @author Ilyas Galiev
* 11-912
* Task 18
*/


using System;

namespace ConsoleApplication1
{
    class Program
    {
        public static void Main(string[] args)
        {
            int n = args.Length;
            int b = 0, c = 0;

            Console.WriteLine($"n={n}");

            for (int i = 0; i < n; i++)
            {
                int tmp = Convert.ToInt32(args[i]);
                while (tmp > 0) // домножение на 10 по количеству разрядов в текущем числе
                {
                    b *= 10;
                    tmp /= 10;
                }

                b += Convert.ToInt32(args[i]);
            }

            Console.WriteLine("b=" + b);

            for (int i = n - 1; i >= 0; i--)
            {
                int tmp = Convert.ToInt32(args[i]);
                while (tmp > 0)
                {
                    c *= 10;
                    tmp /= 10;
                }

                c += Convert.ToInt32(args[i]);
            }

            Console.WriteLine("c=" + c);
        }
    }
}