﻿/**
* @author Ilyas Galiev
* 11-912
* Task 19
*/


using System;

namespace ConsoleApplication1
{
    class Program
    {
        public static void Main(string[] args)
        {
            Console.Write("Enter array size: ");
            int n = Convert.ToInt32(Console.ReadLine());


            int[] a1 = new int[n];
            int[] a2 = new int[n];
            int[] a3 = new int[n];
            int[] a4 = new int[n];
            int[] a5 = new int[n];
            Console.Write("Первая строка первой подстановки: ");
            for (int i = 0; i < n; i++)
                do
                {
                    a1[i] = Convert.ToInt32(Console.ReadLine());
                } while (a1[i] > n - 1);

            Console.Write("Вторая строка первой подстановки: ");
            for (int i = 0; i < n; i++)
                do
                {
                    a2[i] = Convert.ToInt32(Console.ReadLine());
                } while (a2[i] > n - 1);

            Console.Write("Первая строка второй подстановки: ");
            for (int i = 0; i < n; i++)
                do
                {
                    a3[i] = Convert.ToInt32(Console.ReadLine());
                } while (a3[i] > n - 1);

            Console.Write("Вторая строка второй подстановки: ");
            for (int i = 0; i < n; i++)
                do
                {
                    a4[i] = Convert.ToInt32(Console.ReadLine());
                } while (a4[i] > n - 1);

            for (int i = 0; i < n; i++)
            {
                int i1 = 0;
                for (int j = 0; j < n; j++)
                {
                    if (a3[j] == i)
                    {
                        i1 = j;
                        break;
                    }
                }

                i1 = a4[i1];
                int i2 = 0;
                for (int j = 0; j < n; j++)
                {
                    if (a1[j] == i1)
                    {
                        i2 = j;
                        break;
                    }
                }

                a5[i] = a2[i2];
            }

            Console.Write("Result: ");
            for (int i = 0; i < n; i++)
            {
                a1[i] = a5[i];
                Console.Write(a5[i] + " ");
            }
        }
    }
}