﻿/**
* @author Ilyas Galiev
* 11-912
* Task 21
*/
using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter array size:");
            int count = Convert.ToInt32(Console.ReadLine());

            int[] arr = new int[count];
            Console.WriteLine("Enter array:");
            for (int i = 0; i < count; i++)
            {
                arr[i] = Convert.ToInt32(Console.ReadLine());
                Console.Write($"{arr[i]} ");
            }


            Console.WriteLine();
            Console.Write("Enter element for search: ");


            int search = Convert.ToInt32(Console.ReadLine());

            bool flag = false;
            int l = 0, r = count - 1, mid = 0;
            while ((l <= r) && (flag != true))
            {
                mid = (l + r) / 2;
                if (arr[mid] == search) flag = true;
                if (arr[mid] > search) r = mid - 1;
                else l = mid + 1;
            }

            if (flag) Console.WriteLine($"arr[{mid}] = {search}");
            else Console.WriteLine("Не найдено");
        }
    }
}