﻿/**
* @author Ilyas Galiev
* 11-912
* Task 17
*/


using System;

namespace ConsoleApplication1
{
    class Program
    {
        public static void Main(string[] args)
        {
            Console.Write("Enter array size: ");
            int n = Convert.ToInt32(Console.ReadLine());
            int[] a = new int[n];
            int k = 1;
            for (int i = 1; i <= n; i++)
            {
                a[i - 1] = 2 * i - 1;
                a[i - 1] *= k;
                k *= -1;
                Console.Write(a[i-1] + " ");
            }
        }
    }
}
