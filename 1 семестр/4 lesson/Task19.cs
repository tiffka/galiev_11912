﻿/**
* @author Ilyas Galiev
* 11-912
* Task 19
*/


using System;

namespace ConsoleApplication1
{
    class Program
    {
        public static void Main(string[] args)
        {
            Console.Write("Enter array size: ");
            int n = Convert.ToInt32(Console.ReadLine());
            
            
            int[] a1 = new int[n];
            int[] a2 = new int[n];
            int[] a3 = new int[n];
            Console.Write("Enter the first array: ");
            for (int i = 0; i < n; i++)
                do
                {
                    a1[i] = Convert.ToInt32(Console.ReadLine());
                } while (a1[i] > n - 1);
            Console.Write("Enter the second array: ");
            for (int i = 0; i < n; i++)
                do
                {
                    a2[i] = Convert.ToInt32(Console.ReadLine());
                } while (a2[i] > n - 1);

            for (int i = 0; i < n; i++)
            {
                a3[i] = a1[a2[i]];
            }
            Console.Write("Result: ");
            for (int i = 0; i < n; i++)
            {
                a1[i] = a3[i];
                Console.Write(a1[i] + " ");
            }
            
        }
    }
}