﻿/**
* @author Ilyas Galiev
* 11-912
* Task 20
*/

using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter array size:");
            int count = Convert.ToInt32(Console.ReadLine());

            int[] arr = new int[count];
            Console.WriteLine("Enter array:");
            for (int i = 0; i < count; i++)
            {
                arr[i] = Convert.ToInt32(Console.ReadLine());
            }

            bool flag = true;
            for (int i = 0; i < count - 1; i++)
            for (int j = i + 1; j < count; j++)
            {
                if (arr[i] == arr[j])
                    flag = false;
            }

            Console.WriteLine(flag ? "Yes" : "No");
        }
    }
}