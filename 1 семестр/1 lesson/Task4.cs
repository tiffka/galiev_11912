﻿using System;

namespace ConsoleApp1
{
    public class Program
    {
        public static void Main()
        {
            int day = 28, month = 2, year = 2019;
            if (month == 1
                || month == 3
                || month == 5
                || month == 7
                || month == 8
                || month == 10
            )
            {
                if (day == 31)
                {
                    day = 1;
                    month++;
                }
                else day++;
            }

            if (month == 2)
            {
                if (day == 28)
                {
                    if (year % 400 == 0 || (year % 4 == 0 && year % 100 != 0)) day = 29;
                    else
                    {
                        day = 1;
                        month = 3;
                    }
                }
                else if (day == 29)
                {
                    day = 1;
                    month = 3;
                }
                else day++;
            }

            if (month == 4 || month == 6 || month == 11)
            {
                if (day == 30)
                {
                    day = 1;
                    month++;
                }
                else day++;
            }
            if (month == 12)
            {
                if (day == 31)
                {
                    day = 1;
                    month = 1;
                    year++;
                }
                else day++;
            }
            Console.WriteLine("{0}.{1}.{2}", day, month, year);
        }
    }
}