﻿using System;

namespace Task1
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 10, b = 5, max;
            if (a > b) max = a;
            else max = b;
            Console.Write("Max: {0}", max);
        }
    }
}