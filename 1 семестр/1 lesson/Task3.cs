﻿using System;

namespace ConsoleApp1
{
    public class Program
    {
        public static void Main()
        {
            int a = 1, b = 4, c = -5;
            double D, x1, x2;
            D = b * b - 4 * a * c;
            if (D >= 0)
            {
                x1 = (-b + Math.Sqrt(D)) / (2 * a);
                x2 = (-b - Math.Sqrt(D)) / (2 * a);
                Console.WriteLine("x1 = {0}", x1);
                Console.WriteLine("x2 = {0}", x2);
            }
            else Console.WriteLine("Корней нет");
        }
    }
}