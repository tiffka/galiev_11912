﻿using System;

namespace Task2
{
    class Program
    {
        static void Main(string[] args)
        {
            double x = -1.2, y;
            if (x <= 0) y = -x;
            else if (x <= 2) y = x * x;
            else y = 4;
            Console.WriteLine(y);
        }
    }
}