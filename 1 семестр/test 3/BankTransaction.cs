﻿using System;

namespace ConsoleApplication2
{
    public class BankTransaction
    {
        public readonly DateTime datetime;
        public readonly double sum;

        public BankTransaction()
        {
            throw new Exception("Empty transaction");
        }

        public BankTransaction(double sum)
        {
            this.sum = sum;
            this.datetime = DateTime.Now;
        }
    }
}