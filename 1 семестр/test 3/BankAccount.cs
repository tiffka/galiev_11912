﻿using System;

namespace ConsoleApplication2
{
    public class BankAccount : IPrint
    {
        private static int autoIncrement = 1000;
		
        private int accountNumber;
        private double balance;
        private Person owner;
        private BankTransaction[] transactions;
        private int transactionsCount = 0;

        public double Balance => balance;
        public int AccountNumber => accountNumber;
        public Person Owner => owner;
        public BankTransaction[] Transactions => transactions;

        public BankTransaction this[double sum]
        {
            get
            {
                for (int i = 0; i <= transactionsCount; i++)
                {
                    if (transactions[i]?.sum == sum)
                        return transactions[i];
                }

                return null;
            }
        }

        public BankTransaction this[DateTime dt]
        {
            get
            {
                for (int i = 0; i <= transactionsCount; i++)
                {
                    if (transactions[i]?.datetime == dt)
                        return transactions[i];
                }

                return null;
            }
        }

        public BankAccount(Person owner)
        {
            this.owner = owner;
            this.balance = 0.0;
            this.accountNumber = autoIncrement;
            transactions = new BankTransaction[20];

            autoIncrement++;
        }

        public BankAccount()
        {
            throw new Exception("Bank account must has owner");
        }

        public void Deposit(double sum)
        {
            if (transactionsCount == 19) throw new Exception("Too many transactions");

            balance += sum;

            transactions[transactionsCount] = new BankTransaction(sum);
            transactionsCount++;
        }

        public virtual bool Withdraw(double sum)
        {
            if (transactionsCount == 19) throw new Exception("Too many transactions");

            if (balance - sum < 0.0) return false;
            balance -= sum;

            transactions[transactionsCount] = new BankTransaction(sum * -1);
            transactionsCount++;
            return true;
        }

        public bool Transfer(BankAccount account, double sum)
        {
            if (Withdraw(sum))
            {
                account.Deposit(sum);
                return true;
            }

            return false;
        }

        public void Print()
        {
            Console.WriteLine(this);
        }


        public override string ToString()
        {
            return "Bank account # " + AccountNumber + " \nOwner: " + Owner + "\nBalance: " + Balance;
        }

        public static bool operator ==(BankAccount account1, BankAccount account2)
        {
            return account1.AccountNumber == account2.AccountNumber;
        }

        public static bool operator !=(BankAccount account1, BankAccount account2)
        {
            return !(account1 == account2);
        }

        public override bool Equals(Object obj)
        {
            BankAccount account = (BankAccount) obj;
            return account.AccountNumber == AccountNumber;
        }
    }
}