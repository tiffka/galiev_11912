﻿namespace ConsoleApplication2
{
    public class Person
    {
        private string name, passport, phone;

        public string Name
        {
            get => name;
            set => name = value;
        }

        public string Passport
        {
            get => passport;
            set => passport = value;
        }

        public string Phone
        {
            get => phone;
            set => phone = value;
        }

        public Person(string name, string passport, string phone)
        {
            Name = name;
            Passport = passport;
            Phone = phone;
        }

        public Person() : this("", "", "")
        {
        }

        public override string ToString()
        {
            return Name + ", passport: " + Passport + ", phone: " + Phone;
        }
    }
}