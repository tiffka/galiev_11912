﻿using System;

namespace ConsoleApplication2
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Person person = new Person("Ilyas", "9215 0000000", "+79531234567");
            Console.WriteLine(person);

            BankAccount account1 = new BankAccount(person);
            BankAccount account2 = new BankAccount(person);

            account1.Print();
            Console.WriteLine(account2.AccountNumber);

            account1.Deposit(1000);
            account2.Deposit(500);

            Console.WriteLine(account2.Withdraw(1000) ? "Withdraw success" : "Withdraw fail");

            Console.WriteLine(account1.Transfer(account2, 400) ? "Transfer success" : "Transfer fail");
            Console.WriteLine(account1.Balance);
            Console.WriteLine(account2.Balance);

            Console.WriteLine("Transactions:");
            DateTime dt = account1.Transactions[0].datetime;
            Console.WriteLine(account1[1000]?.datetime);
            Console.WriteLine(account1[dt]?.sum);

            Console.WriteLine("Equals:");
            Console.WriteLine(account1 == account1);
            Console.WriteLine(account1 == account2);
            Console.WriteLine(account1.Equals(account2));

            SavingsBankAccount saving = new SavingsBankAccount(person, 10, 100000, true);
            SavingsBankAccount saving2 = new SavingsBankAccount(person, 10, 100000, false);

            Console.WriteLine("Savings bank account:");
            saving.Deposit(1000000);

            saving.Grow();
            Console.WriteLine(saving.Balance);

            Console.WriteLine(saving.Withdraw(5000));
            Console.WriteLine(saving.Withdraw(150000));
            Console.WriteLine(saving2.Withdraw(150000));

            Console.WriteLine(saving.Balance);

            Console.ReadKey();
        }
    }
}