﻿namespace ConsoleApplication2
{
    public class SavingsBankAccount : BankAccount
    {
        private double interestPercent;
        private bool withdrawAllowed;
        private double maxWithdrawSum;

        public double InterestPercent => interestPercent;
        public bool WithdrawAllowed => withdrawAllowed;
        public double MaxWithdrawSum => maxWithdrawSum;

        public SavingsBankAccount(Person owner, double interestPercent, double maxWithdrawSum, bool withdrawAllowed) :
            base(owner)
        {
            this.interestPercent = interestPercent;
            this.maxWithdrawSum = maxWithdrawSum;
            this.withdrawAllowed = withdrawAllowed;
        }

        public SavingsBankAccount(Person owner) : this(owner, 5.0, 10000, true)
        {
        }


        public override bool Withdraw(double sum)
        {
            if (!WithdrawAllowed || MaxWithdrawSum < sum)
            {
                return false;
            }

            return base.Withdraw(sum);
        }

        public void Grow()
        {
            double grow = Balance * (InterestPercent / 100);
            Deposit(grow);
        }
    }
}