﻿/**
* @author Ilyas Galiev
* 11-912
* Task 31
*/

using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Array mxnxz. Enter m: ");
            Int32.TryParse(Console.ReadLine(), out int m);
            Console.Write("Enter n: ");
            Int32.TryParse(Console.ReadLine(), out int n);
            Console.Write("Enter z: ");
            Int32.TryParse(Console.ReadLine(), out int z);

            int[,,] a = new int[m, n, z];

            Console.WriteLine("Press 1 for filling array by random, another - by keyboard");
            int randFill = Int32.Parse(Console.ReadLine());

            Random rand = new Random();
            if (randFill == 1)
                for (int i = 0; i < m; i++)
                for (int j = 0; j < n; j++)
                for (int k = 0; k < z; k++)
                    a[i, j, k] = rand.Next(0, 9);
            else
            {
                Console.WriteLine("Enter arrays:");
                for (int i = 0; i < m; i++)
                for (int j = 0; j < n; j++)
                for (int k = 0; k < z; k++)
                    Int32.TryParse(Console.ReadLine(), out a[i, j, k]);
            }

            
            Console.WriteLine("Array:");
            for (int i = 0; i < m; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    for (int k = 0; k < z; k++)
                        Console.Write(a[i, j, k] + " ");
                    Console.WriteLine();
                }
                Console.WriteLine();
            }
            
            bool flag = true;

            for (int i = 0; i < m; i++)
            {
                bool flagOne = false;
                for (int j = 0; j < n; j++)
                {
                    int[] tmp = new int[z];
                    for (int k = 0; k < z; k++)
                    {
                        tmp[k] = a[i, j, k];
                    }

                    if (checkDiv3(tmp)) flagOne = true;
                }

                if (!flagOne) flag = false;
            }

            Console.WriteLine(flag ? "Yes" : "No");
        }

        static bool checkDiv3(int[] a)
        {
            bool flag = false;

            for (int i = 0; i < a.Length; i++)
            {
                if (a[i] % 3 == 0) flag = true;
            }

            return flag;
        }
    }
}