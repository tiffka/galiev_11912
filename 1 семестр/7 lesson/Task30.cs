﻿/**
* @author Ilyas Galiev
* 11-912
* Task 30
*/

using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("enter matrix size: ");
            int m = Int32.Parse(Console.ReadLine());
            int n = m;

            double[,] a = new Double[m, n];

            Console.WriteLine("Press 1 for filling matrix by random, another - by keyboard");
            int randFill = Int32.Parse(Console.ReadLine());
            if (randFill == 1)
                fillMatrixRand(a);
            else
                fillMatrix(a);

            printMatrix(a);
            makeStepView(a);
            printMatrix(a);
        }

        static void makeStepView(double[,] a)
        {
            int m = a.GetLength(0);
            int n = a.GetLength(1);

            int firstNotNullString = -1;
            for (int i = 0; i < n; i++)
            {
                for (int j = i; j < m; j++)
                    if (a[j, i] != 0)
                    {
                        firstNotNullString = j;
                        break;
                    }

                if (firstNotNullString == -1) continue;

                if (firstNotNullString != 0)
                    for (int j = i; j < m; j++)
                    {
                        double tmp = a[i, j];
                        a[i, j] = a[firstNotNullString, j];
                        a[firstNotNullString, j] = tmp;
                    }

                for (int j = i + 1; j < m; j++)
                {
                    double firstCoff = -a[j, i] / a[i, i];
                    for (int k = i; k < n; k++)
                        a[j, k] += a[i, k] * firstCoff;
                }
            }
        }

        static void fillMatrixRand(double[,] a)
        {
            Random rnd = new Random();
            for (int i = 0; i < a.GetLength(0); i++)
            for (int j = 0; j < a.GetLength(1); j++)
                a[i, j] = rnd.Next(1, 9);
        }

        static void fillMatrix(double[,] a)
        {
            Console.WriteLine("Fill matrix: ");
            for (int i = 0; i < a.GetLength(0); i++)
            for (int j = 0; j < a.GetLength(1); j++)
                Double.TryParse(Console.ReadLine(), out a[i, j]);
        }

        static void printMatrix(double[,] a)
        {
            Console.WriteLine("Matrix:");
            for (int i = 0; i < a.GetLength(0); i++)
            {
                for (int j = 0; j < a.GetLength(1); j++)
                    Console.Write(a[i, j] + " ");
                Console.WriteLine();
            }
        }
    }
}