﻿/**
* @author Ilyas Galiev
* 11-912
* Task 29
*/

using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("enter matrix size: ");
            Int32.TryParse(Console.ReadLine(), out int m);
            int n = m;

            int[,] a = new int[2*m+1, 2*n+1];

            Console.WriteLine("Press 1 for filling matrix by random, another - by keyboard");
            int randFill = Int32.Parse(Console.ReadLine());
            if (randFill == 1)
                fillMatrixRand(a);
            else
                fillMatrix(a);

            printMatrix(a);
            makeZero(a);
            printMatrix(a);
        }

        static void makeZero(int[,] a)
        {
            int m = a.GetLength(0);
            int n = a.GetLength(1);

            for (int i = 0; i < m; i++)
            for (int j = 0; j < n; j++)
                if (j > i && j < (n - i - 1) || j < i && j > (n - i - 1))
                    a[i, j] = 0;
        }

        static void fillMatrixRand(int[,] a)
        {
            Random rnd = new Random();
            for (int i = 0; i < a.GetLength(0); i++)
            for (int j = 0; j < a.GetLength(1); j++)
                a[i, j] = rnd.Next(1, 9);
        }

        static void fillMatrix(int[,] a)
        {
            Console.WriteLine("Fill matrix: ");
            for (int i = 0; i < a.GetLength(0); i++)
            for (int j = 0; j < a.GetLength(1); j++)
                Int32.TryParse(Console.ReadLine(), out a[i, j]);
        }

        static void printMatrix(int[,] a)
        {
            Console.WriteLine("Matrix:");
            for (int i = 0; i < a.GetLength(0); i++)
            {
                for (int j = 0; j < a.GetLength(1); j++)
                    Console.Write(a[i, j] + " ");
                Console.WriteLine();
            }
        }
    }
}