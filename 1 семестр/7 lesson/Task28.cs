﻿/**
* @author Ilyas Galiev
* 11-912
* Task 28
*/
using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("enter matrix size: ");
            Int32.TryParse(Console.ReadLine(), out int m);
            int n = m;

            int[,] a = new int[m, n];

            Console.WriteLine("Press 1 for filling matrix by random, another - by keyboard");
            int randFill = Int32.Parse(Console.ReadLine());
            if (randFill == 1)
                fillMatrixRand(a);
            else
                fillMatrix(a);

            printMatrix(a);

            bool evenSum = checkSum(a);
            Console.WriteLine(evenSum ? "Yes" : "No");
        }

        static bool checkSum(int[,] a)
        {
            bool flag = true;
            int m = a.GetLength(0);
            int n = a.GetLength(1);

            for (int i = m - 1; i >= 0; i--)
            {
                int sum = 0;
                int col = i;

                for (int j = 0; j < n; j++, col--)
                {
                    if (col < 0) col = n - 1;
                    sum += a[i, col];
                }

                if (sum % 2 != 0) flag = false;
            }


            return flag;
        }

        static void fillMatrixRand(int[,] a)
        {
            Random rnd = new Random();
            for (int i = 0; i < a.GetLength(0); i++)
            for (int j = 0; j < a.GetLength(1); j++)
                a[i, j] = rnd.Next(0, 9);
        }

        static void fillMatrix(int[,] a)
        {
            Console.WriteLine("Fill matrix: ");
            for (int i = 0; i < a.GetLength(0); i++)
            for (int j = 0; j < a.GetLength(1); j++)
                 Int32.TryParse(Console.ReadLine(), out a[i, j]);
        }

        static void printMatrix(int[,] a)
        {
            Console.WriteLine("Matrix:");
            for (int i = 0; i < a.GetLength(0); i++)
            {
                for (int j = 0; j < a.GetLength(1); j++)
                    Console.Write(a[i, j] + " ");
                Console.WriteLine();
            }
        }
    }
}