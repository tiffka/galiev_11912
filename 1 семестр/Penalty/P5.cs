﻿/**
* @author Ilyas Galiev
* 11-912
* Task P5
*/

using System;

namespace ConsoleApplication3
{
    class Program
    {
        static void Main()
        {
            Int32.TryParse(Console.ReadLine(), out int n);
            int[] a = new int[n];

            for (int i = 0; i < n; i++)
                Int32.TryParse(Console.ReadLine(), out a[i]);

            for (int i = 0; i < n; i++)
            {
                Console.Write($"{a[i]} ");
            }
            Console.WriteLine();

            Int32.TryParse(Console.ReadLine(), out int k);


            for (int i = 0; i < k; i++)
            {
                int last = a[n-1];
                for (int j = n-2; j >= 0; j--)
                {
                    a[j + 1] = a[j];
                }

                a[0] = last;
            }
            
            for (int i = 0; i < n; i++)
            {
                Console.Write($"{a[i]} ");
            }
            Console.WriteLine();

        }
    }
}