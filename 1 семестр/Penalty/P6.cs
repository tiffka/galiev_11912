﻿/**
* @author Ilyas Galiev
* 11-912
* Task P6
*/

using System;

namespace ConsoleApplication3
{
    class Program
    {
        static void Main()
        {
            Int32.TryParse(Console.ReadLine(), out int n);
            string[] teams = new string[n];
            int[] diff = new int[n];

            for (int i = 0; i < n; i++)
            {
                teams[i] = Console.ReadLine();
                diff[i] = 0;
            }

            Int32.TryParse(Console.ReadLine(), out int k);

            for (int i = 0; i < k; i++)
            {
                string res = Console.ReadLine();
                string[] parsed = res?.Split(' ');
                string[] score = parsed?[2]?.Split(':');

                int team1Index = GetTeamIndex(teams, parsed?[0]);
                int team2Index = GetTeamIndex(teams, parsed?[1]);

                if (team1Index < 0 || team2Index < 0) continue;
                diff[team1Index] += Convert.ToInt32(score?[0]) - Convert.ToInt32(score?[1]);
                diff[team2Index] += Convert.ToInt32(score?[1]) - Convert.ToInt32(score?[0]);
            }


            for (int i = 0; i < n; i++)
            {
                Console.WriteLine(teams[i] + " - разница забитых и пропущенных мячей: " + diff[i]);
            }
        }

        static int GetTeamIndex(string[] teams, string team)
        {
            for (int i = 0; i < teams.Length; i++)
            {
                if (teams[i] == team) return i;
            }

            return -1;
        }
    }
}