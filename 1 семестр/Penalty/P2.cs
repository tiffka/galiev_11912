﻿/**
* @author Ilyas Galiev
* 11-912
* Task P2
*/

using System;

namespace ConsoleApplication3
{
    class Program
    {
        static void Main()
        {
            Int32.TryParse(Console.ReadLine(), out int n);
            int[,] matr = new int[n, n];

            for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
                matr[i, j] = 1;
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++) Console.Write($"{matr[i, j]} ");
                Console.WriteLine();
            }


            Console.WriteLine();

            Print(matr);
        }

        static void Print(int[,] matr)
        {
            int n = matr.GetLength(0);
            int avg = n / 2 + 1;

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    int k = i + 1;
                    if (i + 1 > avg) k = n - i;

                    int count = 2 * k - 1;

                    if (j + 1 > (n - count) / 2 && j < n - (n - count) / 2)
                    {
                        Console.Write($"{matr[i, j]} ");
                    }
                    else Console.Write("0 ");
                }

                Console.WriteLine();
            }
        }
    }
}