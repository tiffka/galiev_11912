﻿/**
* @author Ilyas Galiev
* 11-912
* Task P1
*/

using System;

namespace ConsoleApplication3
{
    class Program
    {
        static void Main()
        {
            Int32.TryParse(Console.ReadLine(), out int n);
            int[,] matr = new int[n, n];

            for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
                Int32.TryParse(Console.ReadLine(), out matr[i, j]);
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++) Console.Write($"{matr[i, j]} ");
                Console.WriteLine();
            }

            

            Console.WriteLine(IsTriangle(matr) ? "Матрица треугольная" : "Матрица не треугольная");
        }

        static bool IsTriangle(int[,] matr)
        {
            int n = matr.GetLength(0);
            bool flagDown = true;
            bool flagUp = true;
            for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
            {
                if (i > j && matr[i, j] != 0)
                    flagUp = false;

                if (i < j && matr[i, j] != 0)
                    flagDown = false;
            }

            return flagDown || flagUp;
        }
    }
}