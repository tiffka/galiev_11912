﻿/**
* @author Ilyas Galiev
* 11-912
* Task P4
*/

using System; 

namespace ConsoleApplication3
{
    class Program
    {
        static void Main()
        {
            Int32.TryParse(Console.ReadLine(), out int n);
            Int32.TryParse(Console.ReadLine(), out int m);
            int[,] matr = new int[n, m];

            for (int i = 0; i < n; i++)
            for (int j = 0; j < m; j++)
                Int32.TryParse(Console.ReadLine(), out matr[i, j]);

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++) Console.Write($"{matr[i, j]} ");
                Console.WriteLine();
            }

            ChangeCols(matr);

            Console.WriteLine();
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++) Console.Write($"{matr[i, j]} ");
                Console.WriteLine();
            } 

        }

        static void ChangeCols(int[,] matr)
        {
            int n = matr.GetLength(0);
            int m = matr.GetLength(1);
            int maxSum = 0;
            int maxSumCol = 0;
            int minSum = Int32.MaxValue;
            int minSumCol = 0;
            for (int i = 0; i < m; i++)
            {
                int sum = 0;
                for (int j = 0; j < n; j++)
                {
                    sum += matr[j, i];
                }

                if (sum > maxSum)
                {
                    maxSumCol = i;
                    maxSum = sum;
                }

                if (sum < minSum)
                {
                    minSumCol = i;
                    minSum = sum;
                }
            }

            for (int i = 0; i < n; i++)
            {
                int tmp = matr[i, maxSumCol];
                matr[i, maxSumCol] = matr[i, minSumCol];
                matr[i, minSumCol] = tmp;
            }
        }
    }
}