﻿/**
* @author Ilyas Galiev
* 11-912
* Task P3
*/

using System;

namespace ConsoleApplication3
{
    class Program
    {
        static void Main()
        {
            Int32.TryParse(Console.ReadLine(), out int n);
            Int32.TryParse(Console.ReadLine(), out int m);
            int[,] matr = new int[n, m];

            for (int i = 0; i < n; i++)
            for (int j = 0; j < m; j++)
                Int32.TryParse(Console.ReadLine(), out matr[i, j]);

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++) Console.Write($"{matr[i, j]} ");
                Console.WriteLine();
            }

            Print(matr);
        }

        static void Print(int[,] matr)
        {
            int n = matr.GetLength(0);
            int m = matr.GetLength(1);
            // проверяем на минимум в строке и максимум в столбце
            for (int i = 0; i < n; i++)
            {
                int min = matr[i, 0];
                int minJ = 0;
                for (int j = 1; j < m; j++)
                {
                    if (matr[i, j] < min)
                    {
                        min = matr[i, j];
                        minJ = j;
                    }
                }

                int max = min;
                for (int j = 0; j < n; j++)
                {
                    if (matr[j, minJ] > max)
                    {
                        max = matr[j, minJ];
                        break;
                    }
                }

                if (max == min) Console.WriteLine($"Седловая точка: ({i}, {minJ})");
            }
            
            // проверяем на минимум в столбце и максимум в строке
            for (int i = 0; i < n; i++)
            {
                int max = matr[i, 0];
                int maxJ = 0;
                for (int j = 1; j < m; j++)
                {
                    if (matr[i, j] > max)
                    {
                        max = matr[i, j];
                        maxJ = j;
                    }
                }

                int min = max;
                for (int j = 0; j < n; j++)
                {
                    if (matr[j, maxJ] < max)
                    {
                        max = matr[j, maxJ];
                        break;
                    }
                }

                if (max == min) Console.WriteLine($"Седловая точка: ({i}, {maxJ})");
            }
        }
    }
}