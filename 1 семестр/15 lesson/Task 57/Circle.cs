﻿/**
* @author Ilyas Galiev
* 11-912
* Task 57
*/
using System;

namespace ConsoleApplication1
{
    public class Circle : Point
    {
        private double r;

        public double R
        {
            get => r;
            set => r = value;
        }

        public Circle() : this(0) {}
        public Circle(double R)
        {
            this.R = R;
        }

        
        public double Area()
        {
            return Math.PI * R * R;
        }
        
        
        public override string ToString()
        {
            return base.ToString() + $", R = {R}";
        }
    }
}