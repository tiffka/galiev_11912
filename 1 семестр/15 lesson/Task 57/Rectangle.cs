﻿/**
* @author Ilyas Galiev
* 11-912
* Task 57
*/

using System;

namespace ConsoleApplication1
{
    public class Rectangle : Point
    {
        private double a, b, c;

        public Rectangle() : this(0, 0, 0)
        {
        }

        public Rectangle(double a, double b, double c)
        {
            this.A = a;
            this.B = b;
            this.C = c;
        }

        public double A
        {
            get => a;
            set => a = value;
        }

        public double B
        {
            get => b;
            set => b = value;
        }


        public double C
        {
            get => c;
            set => c = value;
        }

        public double Area()
        {
            double p = (A + B + C) / 2;
            return Math.Sqrt(p * (p - A) * (p - B) * (p - C));
        }
        
        
        public override string ToString()
        {
            return base.ToString() + $", A = {A}, B = {B}, C = {C}";
        }
    }
}