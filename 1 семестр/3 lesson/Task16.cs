﻿﻿/**
 * @author Ilyas Galiev
 * 11-912
 * Task 16
 */

using System;

namespace ConsoleApp1
{
    public class Program
    {
        public static void Main()
        {
            int number = 1200;
            int k = 3, number10 = 0, i = 0, t = 1;

            while (number > 0)
            {
                if (number % 10 >= k)
                {
                    number10 = -1;
                    break;
                }

                number10 += number % 10 * t;

                i++; //Это зачем?
                t *= k;
                number /= 10;
            }

            if (number10 > -1)
                Console.WriteLine(number10);
            else
                Console.WriteLine("Ошибка в числе");
        }
    }
}