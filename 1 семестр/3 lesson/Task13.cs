﻿/**
 * @author Ilyas Galiev
 * 11-912
 * Task 13
 */

using System;

namespace ConsoleApp1
{
    public class Program
    {
        public static void Main()
        {
            int sum1 = 0, sum2 = 0;
            int number = 442550;
            for (int i = 1; i <= 3; i++)
            {
                sum1 += number % 10;
                number /= 10;
            }
            for (int i = 1; i <= 3; i++)
            {
                sum2 += number % 10;
                number /= 10;
            }
            
            Console.WriteLine(sum1==sum2 ? "yes" : "no");
        }
    }
}