﻿/**
 * @author Ilyas Galiev
 * 11-912
 * Task 15
 */

using System;

namespace ConsoleApp1
{
    public class Program
    {
        public static void Main()
        {
            double y1 = 1.0, y2 = 1.0, eps = 1e-9;
            double x = 1.2, x1 = 1.0;
            int i = 1, k = 1;
            Console.WriteLine(Math.Cos(x));
            do
            {
                k *= -1;
                x1 *= x * x / (2 * i * (2 * i - 1));

                y1 = y2;
                y2 += k * x1;
                i++;
                    
            } while (Math.Abs(y2 - y1) > eps);

            Console.WriteLine($"y={y2}");
        }
    }
}