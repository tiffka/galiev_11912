﻿/**
 * @author Ilyas Galiev
 * 11-912
 * Task 14
 */

using System;

namespace ConsoleApp1
{
    public class Program
    {
        public static void Main()
        {
            double y1 = 0.0, y2 = 0.0, eps = 1e-9;
            double x = 0.9, x1 = 0.9;
            int i = 1, k = -1;
            Console.WriteLine(Math.Atan(x));
            do
            {
                k *= -1;
                y1 = y2;
                y2 += k * x1 / (2 * i - 1);
                x1 = x1 * x * x;
                i++;
            } while (Math.Abs(y2 - y1) > eps);

            Console.WriteLine($"y={y2}");
        }
    }
}