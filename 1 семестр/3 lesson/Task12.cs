﻿/**
 * @author Ilyas Galiev
 * 11-912
 * Task 12
 */

using System;

namespace ConsoleApp1
{
    public class Program
    {
        public static void Main()
        {
            int n = 1234, b;
            bool flag = true;
            b = n % 10;
            n /= 10;

            while (n > 0)
            {
                if (n % 10 <= b)
                {
                    flag = false;
                    break;
                }

                b = n % 10;
                n /= 10;
            }
            Console.WriteLine(flag ? "yes" : "no");
        }
    }
}