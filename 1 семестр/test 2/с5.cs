﻿/**
* @author Ilyas Galiev
* 11-912
* Task С5
*/

using System;

namespace ConsoleApp2
{
    class Program
    {
        static void Main()
        {
            Console.Write("Enter matrix size: ");
            Int32.TryParse(Console.ReadLine(), out int m);
            Int32.TryParse(Console.ReadLine(), out int n);
            int[,] a = new int[m, n];
            FillMatrix(a);
            PrintMatrix(a);
            if (Is3Diagonal(a))
                Console.WriteLine("Матрица является трехдиагональной");
            else 
                Console.WriteLine("Матрица не является трехдиагональной");
        }

        static void FillMatrix(int[,] a)
        {
            int m = a.GetLength(0);
            int n = a.GetLength(1);
            for (int i = 0; i < m; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    Console.Write("Enter matrix element: ");
                    Int32.TryParse(Console.ReadLine(), out a[i, j]);
                }
            }
        }

        static void PrintMatrix(int[,] a)
        {
            int m = a.GetLength(0);
            int n = a.GetLength(1);
            Console.WriteLine("Matrix:");
            for (int i = 0; i < m; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    Console.Write($"{a[i, j]}  ");
                }

                Console.WriteLine();
            }
        }

        static bool Is3Diagonal(int[,] a)
        {
            bool flag = true;
            int m = a.GetLength(0);
            int n = a.GetLength(1);

            for (int i = 0; i < m; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    if (a[i, j] != 0 && i != j && i + 1 != j && j + 1 != i)
                    {
                        flag = false;
                    }
                }
            }

            return flag;
        }
    }
}