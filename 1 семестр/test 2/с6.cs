﻿/**
* @author Ilyas Galiev
* 11-912
* Task C6
*/

using System;

namespace ConsoleApp2
{
    class Program
    {
        static void Main()
        {
            Console.Write("Enter string: ");
            string s = Console.ReadLine();
            char[] c = new char[s.Length];

            for (int i = 0, j = 0; i < s.Length; i++)
            {
                int length = 0;
                for (int k = i; k < s.Length; k++, length++)
                    if (s[k] == ' ')
                        break;

                if (length > 2)
                {
                    for (int l = i; l < i + length; l++, j++)
                        c[j] = s[l];
                    
                    if (j < s.Length - 1)
                    {
                        c[j] = ' ';
                        j++;
                    }
                }


                i += length;
            }

            for (int i = 0; i < s.Length; i++)
            {
                Console.Write(c[i]);
            }
        }
    }
}