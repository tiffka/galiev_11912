﻿/**
* @author Ilyas Galiev
* 11-912
* Task С4
*/

using System;

namespace ConsoleApp2
{
    class Program
    {
        static void Main()
        {
            Console.Write("Enter array size: ");
            Int32.TryParse(Console.ReadLine(), out int n);
            int[] a = new int[n];
            FillArr(a);
            PrintArr(a);

            if (Div3(a))
            {
                Console.WriteLine("Условие делимости на 3 выполнено. Ищем сумму");
                int sum = 0;
                for (int i = 0; i < n; i++)
                {
                    if (a[i] > 0) sum += a[i];
                }

                Console.WriteLine($"Сумма: {sum}");
            }
            else
            {
                Console.WriteLine("Условие делимости на 3 не выполнено. Ищем произведение");
                int mult = 1;
                for (int i = 0; i < n; i++)
                {
                    if (a[i] > 0) mult *= a[i];
                }

                Console.WriteLine($"Произведение: {mult}");
            }
        }

        static void FillArr(int[] a)
        {
            int n = a.Length;
            for (int i = 0; i < n; i++)
            {
                Console.Write("Enter array element: ");
                Int32.TryParse(Console.ReadLine(), out a[i]);
            }
        }

        static void PrintArr(int[] a)
        {
            int n = a.Length;
            Console.WriteLine("Array: ");
            for (int i = 0; i < n; i++)
            {
                Console.Write($"{a[i]} ");
            }

            Console.WriteLine();
        }

        static bool Div3(int[] a)
        {
            bool flag = true;
            int n = a.Length;
            if (n < 3) flag = false;
            for (int i = 0; i < n; i++)
            {
                if ((i + 1) % 3 == 0 && a[i] % 3 != 0) flag = false;
            }

            return flag;
        }
    }
}