﻿﻿/**
* @author Ilyas Galiev
* 11-912
* Task 49
*/
using System;

namespace ConsoleApp2
{
    class ComplexNumber
    {
        public double a, b;

        public ComplexNumber()
        {
            this.a = 0;
            this.b = 0;
        }

        public ComplexNumber(double a, double b)
        {
            this.a = a;
            this.b = b;
        }

        public ComplexNumber Add(ComplexNumber z)
        {
            //исходное число z будет испорчено. 
            //надо создать новый объект, в него поместить результат суммирования и его вернуть
            //return new ComplexNumber(z.a+this.a,z.b+this.b);
            //для вычитания аналогично
            z.a += this.a;
            z.b += this.b;
            return z;
        }

        public void Add2(ComplexNumber z)
        {
            this.a += z.a;
            this.b += z.b;
        }

        public ComplexNumber Sub(ComplexNumber z)
        {
            z.a -= this.a;
            z.b -= this.b;
            return z;
        }

        public void Sub2(ComplexNumber z)
        {
            this.a -= z.a;
            this.b -= z.b;
        }

        public ComplexNumber MultNumber(double number)
        {
            ComplexNumber z = new ComplexNumber(this.a, this.b);
            z.a *= number;
            z.b *= number;
            return z;
        }

        public void MultNumber2(double number)
        {
            this.a *= number;
            this.b *= number;
        }

        public ComplexNumber Mult(ComplexNumber z)
        {
            ComplexNumber res =  new ComplexNumber(z.a, z.b);
            res.a = z.a * this.a - z.b * this.b;
            res.b = z.a * this.b + z.b * this.a;
            return res;
        }

        public void Mult2(ComplexNumber z)
        {
            double tmpA = a;
            this.a = z.a * this.a - z.b * this.b;
            this.b = z.a * this.b + z.b * tmpA;
        }

        public ComplexNumber Div(ComplexNumber z)
        {
            double abs = z.a*z.a + z.b*z.b;
            ComplexNumber res = new ComplexNumber(a, b);
            res.Mult2(new ComplexNumber(z.a, -z.b));
            res.MultNumber2(1.0 / abs);

            return res;
        }

        public void Div2(ComplexNumber z)
        {
            double abs = z.a*z.a + z.b*z.b;
            this.Mult2(new ComplexNumber(z.a, -z.b));
            this.MultNumber2(1.0 / abs);
        }

        public double arg()
        {
            double arg;

            arg = Math.Atan(b / a);
            if (b < 0 && a >= 0) arg += Math.PI;
            if (b < 0 && a < 0) arg -= Math.PI;
            
            return arg;
        }
        public ComplexNumber Pow(double pow)
        {
            double arg = this.arg();
            double abs = this.length();
            ComplexNumber res = new ComplexNumber();

            abs = Math.Pow(abs, pow);
            res.a = Math.Cos(pow * arg) * abs;
            res.b = Math.Sin(pow * arg) * abs;
            return res;
        }

        public bool Equals(ComplexNumber z)
        {
            return this.a == z.a && this.b == z.b;
        }

        public double length()
        {
            return Math.Sqrt(a * a + b * b);
        }

        public override string ToString()
        {
            string sign = this.b > 0 ? "+" : "";
            return "z = " + this.a + sign + this.b + "i";
        }
    }
}