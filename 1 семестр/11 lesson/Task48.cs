﻿/**
* @author Ilyas Galiev
* 11-912
* Task 48
*/

using System;

namespace ConsoleApp2
{
    public class Teacher
    {
        private string fullName, lessonName;

        public Teacher(string fullName, string lessonName)
        {
            this.fullName = fullName;
            this.lessonName = lessonName;
        }

        public void setLessonName(string lessonName)
        {
            this.lessonName = lessonName;
        }

        public override string ToString()
        {
            return this.fullName + ", " + this.lessonName;
        } 

        public void giveGrade(string studentName)
        {
            Random random = new Random();
            int grade = random.Next(2, 6); 

            string gradeName = "";
            switch (grade)
            {
                case 2: gradeName = "неудовлетворительно"; break;
                case 3: gradeName = "удовлетворительно"; break;
                case 4: gradeName = "хорошо"; break;
                case 5: gradeName = "отлично"; break;
            }
            
            Console.WriteLine($"преподаватель {this.fullName} оценил студента с именем {studentName} по предмету {this.lessonName} на оценку {gradeName}");
        }
    }
}