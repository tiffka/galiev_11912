﻿/**
* @author Ilyas Galiev
* 11-912
* Task 55
*/
using System;

namespace ConsoleApp2
{
    public class Player
    {
        private int number;
        private string name;
        private int age;

        private static int AUTO_INCREMENT;

        static Player()
        {
            AUTO_INCREMENT = 1;
        }

        public Player(string name, int age)
        {
            number = AUTO_INCREMENT++;
            this.name = name;
            if(age < 17 || age > 20) throw new Exception("Age must be between 17 - 20");

            this.age = age;
        }
        public string Name
        {
            get => name;
            set => name = value;
        }
        public int Age
        {
            get => age;
            set => age = value;
        }
        public int Number
        {
            get => number;
        }

        public override string ToString()
        {
            return $"Player #{this.number} {this.name} (age: {this.age})";
        }
    }
}