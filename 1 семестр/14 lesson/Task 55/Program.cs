﻿/**
* @author Ilyas Galiev
* 11-912
* Task 55
*/

using System;

namespace ConsoleApp2
{
    class Program
    {
        static void Main()
        {
            Player player = new Player("Ilyas", 18);
            Player player2 = new Player("Alex", 20);
            
            Console.WriteLine(player);
            
            Team team = new Team();
            team.Add(player);
            team.Add(player2);
            
            Console.WriteLine(team["Ilyas"]);
            Console.WriteLine(team[2]);
            team.Remove("Ilyas");
        }
    }

}