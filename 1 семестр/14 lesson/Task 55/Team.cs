﻿﻿/**
* @author Ilyas Galiev
* 11-912
* Task 55
*/
namespace ConsoleApp2
{
    public class Team
    {
        private int N = 0;
        Player[] players;

        public Team()
        {
            players = new Player[10];
        }

		/* Если добавить название команды, то индексатор будет вида this[string team, int number] */

        public Player this[int number]
        {
            get
            {
                for (int i = 0; i < 10; i++)
                {
                    if (players[i]?.Number == number)
                    {
                        return players[i];
                    }
                }

                return null;
            }
        }

        public Player this[string name]
        {
            get
            {
                for (int i = 0; i < 10; i++)
                {
                    if (players[i]?.Name == name)
                    {
                        return players[i];
                    }
                }

                return null;
            }
        }

        public void Add(Player p)
        {
            //а если будет больше 10 игроков? Выбросится исключение - выход за границы миассива
            players[N++] = p;
        }
        public void Remove(string name)
        {
            int k = 0;
            for (int i = 0; i < 10; i++)
            {
                if (players[i]?.Name == name)
                {
                    k = i;
                    players[i] = null;
                }
            }

            for (int i = k+1; i < 10; i++)
            {
                players[i - 1] = players[i];
            }

            players[9] = null;
            N--;
        }
    }
}