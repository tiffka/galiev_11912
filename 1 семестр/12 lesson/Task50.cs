﻿/**
* @author Ilyas Galiev
* 11-912
* Task 50
*/
using System;

namespace ConsoleApp2
{
    public class Vector2D
    {
        private double x, y;

        public double X
        {
            get { return x; }
            set { x = value; }
        }

        public double Y
        {
            get { return y; }
            set { y = value; }
        }


        public Vector2D(double x, double y)
        {
            this.X = x;
            this.Y = y;
        }

        public Vector2D() : this(0, 0)
        {
        }

        public Vector2D Add(Vector2D vector)
        {
            Vector2D vector2 = new Vector2D(vector.X, vector.Y);
            vector2.X += this.X;
            vector2.Y += this.Y;

            return vector;
        }
        public void Add2(Vector2D vector)
        {
            this.X += vector.X;
            this.Y += vector.Y;
        }
        public Vector2D Sub(Vector2D vector)
        {
            Vector2D vector2 = new Vector2D(vector.X, vector.Y);
            vector2.X -= this.X;
            vector2.Y -= this.Y;

            return vector;
        }
        public void Sub2(Vector2D vector)
        {
            this.X -= vector.X;
            this.Y -= vector.Y;
        }

        public Vector2D Mult(double d)
        {
            Vector2D res = new Vector2D(X, Y);
            res.X *= d;
            res.Y *= d;
            return res;
        }
        public void Mult2(double d)
        {
            X *= d;
            Y *= d;
        }

        public double Length()
        {
            return Math.Sqrt(X * X + Y * Y);
        }

        public override string ToString()
        {
            return $"A ({X}, {Y})";
        }

        public double ScalarProd(Vector2D vector)
        {
            return X * vector.X + Y * vector.Y;
        }

        public bool Equals(Vector2D vector)
        {
            return vector.X == X && vector.Y == Y;
        }

        public double Cos(Vector2D vector)
        {
            return ScalarProd(vector) / (this.Length() * vector.Length());
        }
    }
}