﻿/**
* @author Ilyas Galiev
* 11-912
* Task 26
*/

using System;

namespace ConsoleApplication2
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            int m = Convert.ToInt32(Console.ReadLine());
            int n = Convert.ToInt32(Console.ReadLine());
            int[,] a = new int[m, n];

            for (int i = 0; i < m; i++)
            for (int j = 0; j < n; j++)
                a[i, j] = Convert.ToInt32(Console.ReadLine());
            
            int maxSum = 0;
            for (int i = 0; i < m; i++)
            {
                int tempSum = 0;

                int col = i;
                for (int j = 0; j < m; j++, col++)
                {
                    if (col == m)
                        col = 0;

                    tempSum += a[j, col];
                }

                if (tempSum > maxSum) maxSum = tempSum;
            }

            Console.WriteLine($"max = {maxSum}");
        }
    }
}