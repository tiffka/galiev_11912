﻿/**
* @author Ilyas Galiev
* 11-912
* Task 27
*/

using System;

namespace ConsoleApplication2 
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("enter array size: ");
            int m = Convert.ToInt32(Console.ReadLine());
            int n = Convert.ToInt32(Console.ReadLine());
            int[,] a = new int[m, n];

            
            Console.WriteLine("enter first array: ");
            for (int i = 0; i < m; i++)
            for (int j = 0; j < n; j++)
                a[i, j] = Convert.ToInt32(Console.ReadLine());
            
            Console.WriteLine("enter second array size: ");
            int m2 = Convert.ToInt32(Console.ReadLine());
            int n2 = Convert.ToInt32(Console.ReadLine());
            int[,] a2 = new int[m2, n2];

            
            Console.WriteLine("enter second array: ");
            for (int i = 0; i < m2; i++)
            for (int j = 0; j < n2; j++)
                a2[i, j] = Convert.ToInt32(Console.ReadLine());

            int[,] res = new int[m, n2];

            if (n != m2) throw new Exception("error");

            
            Console.WriteLine("Result: ");
            for (int i = 0; i < m; i++)
            {
                for (int j = 0; j < n2; j++)
                {
                    int tempSum = 0;
                    for (int k = 0; k < n; k++)
                    {
                        tempSum += a[i, k] * a2[k, j];
                    }

                    res[i, j] = tempSum;
                    Console.Write($"{res[i, j]} ");
                }

                Console.WriteLine();
            }
        }
    }
}