﻿/**
* @author Ilyas Galiev
* 11-912
* Task 25
*/

using System;

namespace ConsoleApplication2
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            int m = Convert.ToInt32(Console.ReadLine());
            int n = Convert.ToInt32(Console.ReadLine());
            int[,] a = new int[m, n];

            for (int i = 0; i < m; i++)
            for (int j = 0; j < n; j++)
                a[i, j] = Convert.ToInt32(Console.ReadLine());

            int maxSum = 0;
            int[] firstElIndex = new int[2];
            for (int i = 1; i < m; i++)
            {
                int tempSum = 0, tempSum2 = 0;
                for (int j = 0; j < m - i; j++)
                {
                    tempSum += a[j, j + i];
                    tempSum2 += a[j + i, j];
                }

                if (tempSum > maxSum)
                {
                    maxSum = tempSum;

                    firstElIndex[0] = 0;
                    firstElIndex[1] = i;
                }

                if (tempSum2 > maxSum)
                {
                    maxSum = tempSum2;

                    firstElIndex[0] = i;
                    firstElIndex[1] = 0;
                }
            }

            Console.WriteLine($"max = {maxSum}");
            for (int i = 0; i < m - (firstElIndex[0] > firstElIndex[1] ? firstElIndex[0] : firstElIndex[1]); i++)
            {
                Console.Write($"{a[i + firstElIndex[0], i + firstElIndex[1]]} ");
            }
        }
    }
}