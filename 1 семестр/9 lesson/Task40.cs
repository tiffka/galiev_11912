﻿/**
* @author Ilyas Galiev
* 11-912
* Task 40
*/

using System;

namespace ConsoleApp2
{
    class Program
    {
        static void Main()
        {
            string s = Console.ReadLine();
            int count = 0;

            bool space = true;
            for (int i = 0; i < s.Length; i++)
            {
                if (s[i] == ' ') space = true;
                else if (space)
                {
                    space = false;
                    if (s[i] >= 'A' && s[i] <= 'Z') count++;
                }
            }
            
            Console.WriteLine(count);
        }
    }
}