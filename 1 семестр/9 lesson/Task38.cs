﻿/**
* @author Ilyas Galiev
* 11-912
* Task 38
*/

using System;

namespace ConsoleApp2
{
    class Program
    {
        static void Main()
        {
            string s1 = Console.ReadLine();
            string s2 = Console.ReadLine();
            if (Compare(s1, s2))
            {
                Console.WriteLine($"1) {s1}");
                Console.WriteLine($"2) {s2}");
            }
            else
            {
                Console.WriteLine($"1) {s2}");
                Console.WriteLine($"2) {s1}");
            }
        }

        static bool Compare(string s1, string s2)
        {
            int n = s1.Length < s2.Length ? s1.Length : s2.Length;

            bool flag = s1.Length < s2.Length ? true : false;
            for (int i = 0; i < n; i++)
            {
                if (char.ToLower(s1[i]) < char.ToLower(s2[i]))
                {
                    flag = true;
                    break;
                }

                if (char.ToLower(s1[i]) > char.ToLower(s2[i]))
                {
                    break;
                }
            }

            return flag;
        }
    }
}