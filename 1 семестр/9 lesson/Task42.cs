﻿/**
* @author Ilyas Galiev
* 11-912
* Task 43
*/

using System;

namespace ConsoleApp2
{
    class Program
    {
        static void Main()
        {
            string s = Console.ReadLine();
            char[] c = new char[s.Length + s.Length / 4];

            bool flag = false;
            for (int i = 0, j = 0; i < s.Length; i++, j++)
            {
                if (i + 3 < s.Length 
                    && s[i] == 't' 
                    && s[i + 1] == 'r' 
                    && s[i + 2] == 'u' 
                    && s[i + 3] == 'e')
                {
                    if (flag)
                    {
                        c[j] = 'f';
                        c[j + 1] = 'a';
                        c[j + 2] = 'l';
                        c[j + 3] = 's';
                        c[j + 4] = 'e';
                        i += 3;
                        j += 4;
                    }
                    else
                    {
                        flag = true;
                        c[j] = s[i];
                    }
                }
                else
                {
                    c[j] = s[i];
                }
            }

            for (int i = 0; i < s.Length + s.Length / 4; i++)
            {
                Console.Write(c[i]);
            }
        }
    }
}