﻿/**
* @author Ilyas Galiev
* 11-912
* Task 43
*/

using System;

namespace ConsoleApp2
{
    class Program
    {
        static void Main()
        {
            string s = Console.ReadLine();
            char[] c = new char[s.Length];

            for (int i = 0, j = 0; i < s.Length; i++, j++)
            {
                if (i + 2 < s.Length
                    && s[i] == 'm'
                    && s[i + 1] == 'o'
                    && s[i + 2] == 'm'
                    && (i == 0 && i + 3 < s.Length && s[i + 3] == ' '
                        || i == 0 && i + 3 == s.Length
                        || i != 0 && s[i - 1] == ' ' &&
                        (i+3==s.Length || s[i+3]==' ')
                    ))
                {
                    c[j] = 'd';
                    c[j + 1] = 'a';
                    c[j + 2] = 'd';
                    i += 2;
                    j += 2;
                }
                else
                {
                    c[j] = s[i];
                }
            }

            for (int i = 0; i < s.Length; i++)
            {
                Console.Write(c[i]);
            }
        }
    }
}