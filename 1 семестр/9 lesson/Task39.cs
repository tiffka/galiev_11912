﻿/**
* @author Ilyas Galiev
* 11-912
* Task 39
*/

using System;

namespace ConsoleApp2
{
    class Program
    {
        static void Main()
        {
            Console.Write("Enter count of strings:");
            Int32.TryParse(Console.ReadLine(), out int n);
            string[] strings = new string[n];
            for (int i = 0; i < n; i++)
            {
                Console.Write("Enter string:");
                strings[i] = Console.ReadLine();
            }

            for (int i = 0; i < n - 1; i++)
            {
                for (int j = i + 1; j < n; j++)
                {
                    if (!Compare(strings[i], strings[j]))
                    {
                        string tmp = strings[i];
                        strings[i] = strings[j];
                        strings[j] = tmp;
                    }
                }
            }

            for (int i = 0; i < n; i++)
            {
                Console.WriteLine($"{i + 1}) {strings[i]}");
            }
        }

        static bool Compare(string s1, string s2)
        {
            int n = s1.Length < s2.Length ? s1.Length : s2.Length;

            bool flag = s1.Length < s2.Length ? true : false;
            for (int i = 0; i < n; i++)
            {
                if (char.ToLower(s1[i]) < char.ToLower(s2[i]))
                {
                    flag = true;
                    break;
                }

                if (s1[i] > s2[i])
                {
                    break;
                }
            }

            return flag;
        }
    }
}