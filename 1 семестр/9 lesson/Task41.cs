﻿/**
* @author Ilyas Galiev
* 11-912
* Task 41
*/

using System;

namespace ConsoleApp2
{
    class Program
    {
        static void Main()
        {
            string s = Console.ReadLine();
            int[] count = new int[26];

            for (int i = 0; i < 26; i++)
                count[i] = 0;

            for (int i = 0; i < s.Length; i++)
            {
                if (s[i] >= 'a' && s[i] <= 'z') count[(int) s[i] - 97]++;
                if (s[i] >= 'A' && s[i] <= 'Z') count[(int) s[i] - 65]++;
            }

            for (int i = 0; i < 26; i++)
            {
                Console.WriteLine((char) (i+65) + ": " + count[i] + " time(s)");
            }
        }
    }
}