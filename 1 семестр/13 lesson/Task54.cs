﻿/**
* @author Ilyas Galiev
* 11-912
* Task 54
*/

using System;

namespace ConsoleApp2
{
    public class Matrix2x2
    {
        public double[,] matrix = new Double[2, 2];

        public Matrix2x2(double x)
        {
            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    matrix[i, j] = x;
                }
            }
        }

        public Matrix2x2() : this(0)
        {
        }

        public Matrix2x2(double[,] x)
        {
            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    matrix[i, j] = x[i, j];
                }
            }
        }

        public Matrix2x2(double x1, double x2, double x3, double x4)
        {
            matrix[0, 0] = x1;
            matrix[0, 1] = x2;
            matrix[1, 0] = x3;
            matrix[1, 1] = x4;
        }

        public Matrix2x2 Add(Matrix2x2 matrix)
        {
            Matrix2x2 matrix2 = new Matrix2x2(matrix.matrix);
            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    matrix2.matrix[i, j] += matrix.matrix[i, j];
                }
            }

            return matrix2;
        }

        public void Add2(Matrix2x2 matrix)
        {
            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    this.matrix[i, j] += matrix.matrix[i, j];
                }
            }
        }

        public Matrix2x2 Sub(Matrix2x2 matrix)
        {
            Matrix2x2 matrix2 = new Matrix2x2(matrix.matrix);
            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    matrix2.matrix[i, j] -= matrix.matrix[i, j];
                }
            }

            return matrix2;
        }

        public void Sub2(Matrix2x2 matrix)
        {
            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    this.matrix[i, j] -= matrix.matrix[i, j];
                }
            }
        }

        public Matrix2x2 MultNumber(double d)
        {
            Matrix2x2 matrix2 = new Matrix2x2(matrix);
            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    matrix2.matrix[i, j] *= d;
                }
            }

            return matrix2;
        }

        public void MultNumber2(double d)
        {
            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    this.matrix[i, j] *= d;
                }
            }
        }

        public Matrix2x2 Mult(Matrix2x2 matrix)
        {
            Matrix2x2 matrix2 = new Matrix2x2();
            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    for (int k = 0; k < 2; k++)
                    {
                        matrix2.matrix[i, j] += this.matrix[i, k] * matrix.matrix[k, j];
                    }
                }
            }

            return matrix2;
        }

        public void Mult2(Matrix2x2 matrix)
        {
            Matrix2x2 res = new Matrix2x2();
            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    for (int k = 0; k < 2; k++)
                    {
                        res.matrix[i, j] += this.matrix[i, k] * matrix.matrix[k, j];
                    }
                }
            }

            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    this.matrix[i, j] = res.matrix[i, j];
                }
            }
        }

        public void Transpon()
        {
            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < i; j++)
                {
                    double tmp = matrix[i, j];
                    matrix[i, j] = matrix[j, i];
                    matrix[j, i] = tmp;
                }
            }
        }

        public double Det()
        {
            return matrix[0, 0] * matrix[1, 1] - matrix[0, 1] * matrix[1, 0];
        }

        public Matrix2x2 InverseMatrix()
        {
            Matrix2x2 matrix2 = new Matrix2x2(this.matrix);
            double det = Det();


            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    matrix2.matrix[i, j] = matrix[1 - i, 1 - j] / det;
                }
            }

            matrix2.matrix[0, 1] *= -1;
            matrix2.matrix[1, 0] *= -1;
            matrix2.Transpon();
            return matrix2;
        }

        public void Print()
        {
            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    Console.Write(this.matrix[i, j] + " ");
                }

                Console.WriteLine();
            }

            Console.WriteLine();
        }

        public static Matrix2x2 operator +(Matrix2x2 m1, Matrix2x2 m2)
        {
            return m1.Add(m2);
        }

        public static Matrix2x2 operator -(Matrix2x2 m1, Matrix2x2 m2)
        {
            return m1.Sub(m2);
        }

        public static Matrix2x2 operator *(Matrix2x2 m1, Matrix2x2 m2)
        {
            return m1.Mult(m2);
        }

        public static Matrix2x2 operator *(Matrix2x2 m1, double d)
        {
            return m1.MultNumber(d);
        }

        public static Matrix2x2 operator *(double d, Matrix2x2 m1)
        {
            return m1.MultNumber(d);
        }

       
        public static Matrix2x2 operator /(Matrix2x2 m1, double d)
        {
            return m1.MultNumber(1.0/d);
        }

        public static Matrix2x2 operator /(double d, Matrix2x2 m1)
        {
            return m1.MultNumber(1.0/d);
        }

        public static bool operator ==(Matrix2x2 m1, Matrix2x2 m2)
        {
            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    if (m1.matrix[i, j] != m2.matrix[i, j]) return false;
                }
            }

            return true;
        }

        public static bool operator !=(Matrix2x2 m1, Matrix2x2 m2)
        {
            return !(m1 == m2);
        }
    }
}