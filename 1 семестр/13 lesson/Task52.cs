﻿/**
* @author Ilyas Galiev
* 11-912
* Task 52
*/

using System;

namespace ConsoleApp2
{
    class ComplexNumber
    {
        public double a, b;

        public double A
        {
            get { return a; }
            set { a = value; }
        }

        public double B
        {
            get { return b; }
            set { b = value; }
        }


        public ComplexNumber() : this(0, 0)
        {
        }

        public ComplexNumber(double a, double b)
        {
            this.A = a;
            this.B = b;
        }

        public ComplexNumber Add(ComplexNumber z)
        {
            ComplexNumber res = new ComplexNumber(A, B);
            res.A += this.A;
            res.B += this.B;
            return res;
        }

        public void Add2(ComplexNumber z)
        {
            this.A += z.A;
            this.B += z.B;
        }

        public ComplexNumber Sub(ComplexNumber z)
        {
            ComplexNumber res = new ComplexNumber(A, B);
            res.A -= this.A;
            res.B -= this.B;
            return z;
        }

        public void Sub2(ComplexNumber z)
        {
            this.A -= z.A;
            this.B -= z.B;
        }

        public ComplexNumber MultNumber(double number)
        {
            ComplexNumber z = new ComplexNumber(this.A, this.B);
            z.A *= number;
            z.B *= number;
            return z;
        }

        public void MultNumber2(double number)
        {
            this.A *= number;
            this.B *= number;
        }

        public ComplexNumber Mult(ComplexNumber z)
        {
            ComplexNumber res = new ComplexNumber(z.A, z.B);
            res.A = z.A * this.A - z.B * this.B;
            res.B = z.A * this.B + z.B * this.A;
            return res;
        }

        public void Mult2(ComplexNumber z)
        {
            double tmpA = a;
            this.A = z.A * this.A - z.B * this.B;
            this.B = z.A * this.B + z.B * tmpA;
        }

        public ComplexNumber Div(ComplexNumber z)
        {
            double abs = z.A * z.A + z.B * z.B;
            ComplexNumber res = new ComplexNumber(a, b);
            res.Mult2(new ComplexNumber(z.A, -z.B));
            res.MultNumber2(1.0 / abs);

            return res;
        }

        public void Div2(ComplexNumber z)
        {
            double abs = z.A * z.A + z.B * z.B;
            this.Mult2(new ComplexNumber(z.A, -z.B));
            this.MultNumber2(1.0 / abs);
        }

        public double Arg()
        {
            double arg;

            arg = Math.Atan(b / a);
            if (b < 0 && a >= 0) arg += Math.PI;
            if (b < 0 && a < 0) arg -= Math.PI;

            return arg;
        }

        public ComplexNumber Pow(double pow)
        {
            double arg = this.Arg();
            double abs = this.length();
            ComplexNumber res = new ComplexNumber();

            abs = Math.Pow(abs, pow);
            res.A = Math.Cos(pow * arg) * abs;
            res.B = Math.Sin(pow * arg) * abs;
            return res;
        }

        public bool Equals(ComplexNumber z)
        {
            return this.A == z.A && this.B == z.B;
        }

        public double length()
        {
            return Math.Sqrt(a * a + b * b);
        }

        public override string ToString()
        {
            string sign = this.B > 0 ? "+" : "";
            if (this.A != 0 && this.B != 0)
                return "z = " + this.A + sign + this.B + "i";
            else if (this.B == 0)
                return "z = " + this.A;
            else if (this.A == 0)
                return "z = " + this.B + "i";
            else return "";
        }

        public static ComplexNumber operator +(ComplexNumber z1, ComplexNumber z2)
        {
            return z1.Add(z2);
        }

        public static ComplexNumber operator +(ComplexNumber z1, double d)
        {
            return new ComplexNumber(z1.A + d, z1.B);
        }

        public static ComplexNumber operator +(double d, ComplexNumber z1)
        {
            return z1 + d;
        }

        public static ComplexNumber operator -(ComplexNumber z1, ComplexNumber z2)
        {
            return z1.Sub(z2);
        }

        public static ComplexNumber operator -(ComplexNumber z1, double d)
        {
            return new ComplexNumber(z1.A - d, z1.B);
        }

        public static ComplexNumber operator -(double d, ComplexNumber z1)
        {
            return new ComplexNumber(-z1.A + d, -z1.B);
        }

        public static ComplexNumber operator *(ComplexNumber z1, ComplexNumber z2)
        {
            return z1.Mult(z2);
        }

        public static ComplexNumber operator *(ComplexNumber z1, double d)
        {
            return z1.MultNumber(d);
        }

        public static ComplexNumber operator *(double d, ComplexNumber z1)
        {
            return z1.MultNumber(d);
        }

        public static ComplexNumber operator /(ComplexNumber z1, ComplexNumber z2)
        {
            return z1.Div(z2);
        }

        public static ComplexNumber operator /(ComplexNumber z1, double d)
        {
            return z1.MultNumber(1.0 / d);
        }
    }
}