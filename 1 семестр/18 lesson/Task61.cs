﻿/**
* @author Ilyas Galiev
* 11-912
* Task 61
*/
using System;

namespace ConsoleApplication1
{
    struct Dot
    {
        public double X, Y;
    }

    internal class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Введите количество точек");
            Int32.TryParse(Console.ReadLine(), out int N);
            Dot[] route = new Dot[N];
            Dot[] path = new Dot[N];

            Console.WriteLine("Введите точки маршрута в формате X;Y");
            for (int i = 0; i < N; i++)
            {
                string[] s = Console.ReadLine().Split(';');
                route[i].X = Convert.ToDouble(s[0]);
                route[i].Y = Convert.ToDouble(s[1]);
            }

            Console.WriteLine("Введите точки пути в формате X;Y");
            for (int i = 0; i < N; i++)
            {
                string[] s = Console.ReadLine().Split(';');
                path[i].X = Convert.ToDouble(s[0]);
                path[i].Y = Convert.ToDouble(s[1]);
            }
            
            Console.WriteLine(CheckRoute(route, path) ? "Не сходил с маршрута" : "Всё плохо");
        }

        static bool CheckRoute(Dot[] route, Dot[] path)
        {
            int N = route.Length;
            for (int i = 0; i < N; i++)
            {
                if (route[i].X != path[i].X || route[i].Y != path[i].Y)
                {
                    return false;
                }
            }
            return true;
        }
    }
}