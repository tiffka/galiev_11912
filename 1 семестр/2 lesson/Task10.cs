﻿﻿/**
 * @author Ilyas Galiev
 * 11-912
 * Task 10
 */

using System;

namespace ConsoleApp1
{
    public class Program
    {
        public static void Main()
        {
            int y = 1, x = 10, i = 1, d = 1, n = 10, fact = 1;
            while (i <= n)
            {
                d *= x;
                fact *= i;
                y += d / fact; // здесь будет целочисленное деление. Надо 1) double y, 2) при делении приведение типов в числителе или знаменателе выполнить
                i++;
            }
            Console.WriteLine($"y={y}");
        }
    }
}