﻿/**
 * @author Ilyas Galiev
 * 11-912
 * Task 11
 */

using System;

namespace ConsoleApp1
{
    public class Program
    {
        public static void Main()
        {
            int p = 1;
            int x = 10, i = 1, d = 1, n = 5, fact = 1;
            while (i <= n)
            {
                d *= x;
                fact *= i;
                p *= (d + fact);
                i++;
            }
            Console.WriteLine($"P={p}");
        }
    }
}