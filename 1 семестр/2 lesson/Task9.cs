﻿/**
 * @author Ilyas Galiev
 * 11-912
 * Task 09
 */

using System;

namespace ConsoleApp1
{
    public class Program
    {
        public static void Main()
        {
            int n = 10, i = 2;
            double p = 1.0;
            while (i <= n)
            {
                p *=  (1 - 1 / (double) i);
                i++;
            }
            Console.WriteLine($"P={p}");
        }
    }
}