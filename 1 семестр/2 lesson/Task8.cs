﻿/**
 * @author Ilyas Galiev
 * 11-912
 * Task 08
 */

using System;

namespace ConsoleApp1
{
    public class Program
    {
        public static void Main()
        {
            int n = 15, line=1, column=1;
            while (line <= 2*n+1)
            {
                column = 1;
                while (column <= 2*n+1)
                {
                    if((column - n-1)*(column-n-1) + (line-n-1)*(line-n-1) <= n*n) Console.Write("0");
                    else Console.Write("*");
                    column++;
                }

                line++;
                Console.WriteLine("");
            }
        }
    }
}