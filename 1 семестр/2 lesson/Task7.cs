﻿/**
 * @author Ilyas Galiev
 * 11-912
 * Task 07
 */

using System;

namespace ConsoleApp1
{
    public class Program
    {
        public static void Main()
        {
            int i = 6, n=30;
            while (i <= n)
            {
                if(i % 3 == 0) Console.Write($"{i} ");
                i++;
            }
        }
    }
}