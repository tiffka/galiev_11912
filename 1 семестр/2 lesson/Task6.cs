﻿/**
 * @author Ilyas Galiev
 * 11-912
 * Task 06
 */

using System;

namespace ConsoleApp1
{
    public class Program
    {
        public static void Main()
        {
            int n = 3, i = 1;
            int k = 2 * n - 1;
            while (i <= n)
            {
                int j = 1;
                while (j <= k / 2 - i + 1)
                {
                    Console.Write(" ");
                    j++;
                }

                j = 1;
                while (j <= 2 * i - 1)
                {
                    Console.Write("*");
                    j++;
                }

                j = 1;
                while (j <= k / 2 - i + 1)
                {
                    Console.Write(" ");
                    j++;
                }

                i++;
                Console.WriteLine("");
            }
        }
    }
}