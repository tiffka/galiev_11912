﻿/**
* @author Ilyas Galiev
* 11-912
* Task 62
*/
using System.Collections.Generic;

namespace ConsoleApp1
{
    class PlayerComparer : IComparer<Player>
    {
        public string compareField = "name";

        public PlayerComparer(string compareField)
        {
            this.compareField = compareField;
        }
        public int Compare(Player p1, Player p2)
        {
            switch (compareField)
            {
                case "name":
                    return p1.Name.CompareTo(p2.Name);
                case "age":
                    if (p1.Age > p2.Age) return 1;
                    else if (p1.Age == p2.Age) return 0;
                    else return -1; 
                case "number":
                    if (p1.Number > p2.Number) return 1;
                    else if (p1.Number == p2.Number) return 0;
                    else return -1;
                default:
                    return p1.Name.CompareTo(p2.Name);
            } 
        }
    }
}