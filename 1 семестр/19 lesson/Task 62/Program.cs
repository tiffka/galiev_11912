﻿/**
* @author Ilyas Galiev
* 11-912
* Task 62
*/

using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Player[] players = new Player[4];
            players[0] = new Player("John", 20);
            players[1] = new Player("Patrick", 18); 
            players[2] = new Player("Kimball", 19);
            players[3] = new Player("Wayne", 17);
            
            Console.WriteLine(players[0].CompareTo(players[3]));
            
            Array.Sort(players, new PlayerComparer("name"));
            Print(players);
            Array.Sort(players, new PlayerComparer("age"));
            Print(players);
            Array.Sort(players, new PlayerComparer("number"));
            Print(players);
        }

        static void Print(Player[] players)
        {
            for (int i = 0; i < 4; i++)
            {
                Console.WriteLine(players[i]);
            }
            Console.WriteLine();
        }

    }
}