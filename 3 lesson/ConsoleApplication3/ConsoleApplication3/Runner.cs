﻿using System;
using List;
using SingletonExample;
using Student;
using Student = Student.Student;

namespace ConsoleApplication3
{
    class Runner
    {
        public static void Run()
        {
            CustomList<int> list = new CustomList<int>();

            list.Add(11);
            list.AddRange(new[] {11, 6, 12, 13, 14, 7, 15, 8, 11, 10, 25, 11});

            list.Display();

            CustomList<int> list2 = new CustomList<int>();
            list2.AddRange(new[] {11, 11, 6, 12, 13, 14, 7, 15, 8, 11, 10, 25, 11});
            Console.WriteLine("list и list2 совпадают: " + list.EqualData(list2));
            list2.Add(10);
            Console.WriteLine("list и list2 совпадают: " + list.EqualData(list2));

            list2.Clear();

            list2.AddRange(new[] {11, 11, 6, 12, 13, 14, 7, 15, 8, 11, 10, 25, 10});
            Console.WriteLine("list и list2 совпадают: " + list.EqualData(list2));

            list2.Clear();
            list2.AddRange(new[] {11, 11, 6, 12, 13, 14, 7, 15, 8, 11, 11, 10, 25, 10, 11, 11});
            Console.WriteLine("Перезапишем список list2: ");
            list2.Display();
            Console.WriteLine("Удаляем все цифры 11 из list2");
            list2.RemoveAll(11);
            list2.Display();


            Console.WriteLine("Удаляем элементы на 0, 15, 3 местах");
            list.RemoveAt(0);
            list.RemoveAt(15);
            list.RemoveAt(3);
            list.Display();


            Console.WriteLine("Вставляем элементы на 0, 4, 40, последнее места");
            list.Insert(0, 5);
            list.Insert(4, -1);
            list.Insert(40, 40);
            list.Insert(list.Size() - 1, 40);
            list.Display();


            Console.WriteLine("Разворачиваем список");
            list.Reverse();
            list.Display();

            Console.WriteLine("Индекс элемента 10");
            Console.WriteLine(list.IndexOf(10));

            Console.WriteLine("Индекс элемента 100");
            Console.WriteLine(list.IndexOf(100));

            Console.WriteLine("Существование элемента 10");
            Console.WriteLine(list.Contains(10));

            Console.WriteLine("Существование элемента 100");
            Console.WriteLine(list.Contains(100));


            Console.WriteLine("Удаляем элементы на 0, 4, 40 местах");
            list.RemoveAt(0);
            list.RemoveAt(4);
            list.RemoveAt(40);
            list.Display();
        }

        public static void RunDouble()
        {
            DoubleList<int> list = new DoubleList<int>();
            list.AddRange(new[] {6, 6, 6, 12, 13, 14, 7, 6, 6, 6, 15, 8, 10, 25, 11, 6, 6});
            list.Display();

            Console.WriteLine("Удаляем числа 6, 11, 7");
            list.Remove(6);
            list.Remove(11);
            list.Remove(7);
            list.Display();

            Console.WriteLine("Удаляем все числа 6");
            list.RemoveAll(6);
            list.Display();


            Console.WriteLine("Удаляем элементы на позициях 0, 5, 15");
            list.RemoveAt(0);
            list.RemoveAt(5);
            list.RemoveAt(15);
            list.Display();

            Console.WriteLine("Вставляем элементы на позиции 0, 5, 15, последняя");
            list.Insert(0, -15);
            list.Insert(5, 150);
            list.Insert(15, 1500);
            list.Insert(list.Size(), 1500);
            list.Display();


            Console.WriteLine("Разворачиваем список");
            list.Reverse();
            list.Display();
        }

        public static void RunArrayList()
        {
            var list = new ArrayList<int>();
            list.AddRange(new int[] {1, 1, 1, 2, 3, 4, 5, 6, 7, 8, 1, 1, 9, 10, 9, 9, 1});
            Console.WriteLine(list);


            list.Remove(1);
            Console.WriteLine(list);
            list.RemoveAll(1);
            Console.WriteLine(list);

            list.Reverse();
            Console.WriteLine(list);

            Console.WriteLine(list.IndexOf(2));
            Console.WriteLine(list.Contains(2));

            list.Insert(0, 1);
            list.Insert(2, -1);
            list.Insert(list.Size()-1, -2);
            list.Insert(list.Size(), -2);
            Console.WriteLine(list);

            list.RemoveAt(0);
            Console.WriteLine(list);
        }

        public static void Student()
        {
            var list = new ArrayList<global::Student.Student>();

            var s1 = new global::Student.Student()
                {Fio = "Ilyas Galiev", Birthday = new DateTime(2001, 5, 8), AverageScore = 54, City = "Kazan"};
            var s2 = new global::Student.Student()
                {Fio = "Bulat Biktagirov", Birthday = new DateTime(2001, 11, 17), AverageScore = 58, City = "Kazan"};
            var s3 = new global::Student.Student()
                {Fio = "Raynur Yakupov", Birthday = new DateTime(2000, 2, 23), AverageScore = 51, City = "Bugulma"};
            var s4 = new global::Student.Student()
                {Fio = "Ilyas Galiev", Birthday = new DateTime(2000, 2, 23), AverageScore = 54, City = "Kazan"};

            list.Add(s1);
            list.Add(s2);
            list.Add(s3);
            list.Add(s4);

            Console.WriteLine("sorted by default");
            list.Sort();
            Console.WriteLine(list);

            list.Sort(new StudentComparer("fio"));
            Console.WriteLine("sorted by fio");
            Console.WriteLine(list);

            list.Sort((p1, p2) => p1.Birthday.CompareTo(p2.Birthday));
            Console.WriteLine("sorted by birthday");
            Console.WriteLine(list);
        }

        public static void Execute()
        {

        }
    }
}