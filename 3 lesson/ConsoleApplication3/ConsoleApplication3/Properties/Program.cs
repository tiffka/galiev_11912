﻿﻿/**
* @author Ilyas Galiev
* 11-912
* Task 57
*/

using System;

namespace ConsoleApplication1
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Figure f = new Figure(false, "white");
            Console.WriteLine(f);
            
            Point p = new Point();
            Console.WriteLine(p);
            p.ChangeColor("white");
            Console.WriteLine(p);
            p.MoveX(2);
            Console.WriteLine(p);

            Circle c = new Circle(2.5);
            Console.WriteLine(c);
            Console.WriteLine(c.Area());

            Rectangle rect = new Rectangle(3, 4, 5);
            Console.WriteLine(rect);
            Console.WriteLine(rect.Area());
        }
    }
}