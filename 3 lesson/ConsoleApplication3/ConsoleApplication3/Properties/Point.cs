﻿﻿/**
* @author Ilyas Galiev
* 11-912
* Task 57
*/

namespace ConsoleApplication1
{
    public class Point : Figure
    {
        private double x, y;

        public double X
        {
            get => x;
            set => x = value;
        }

        public double Y
        {
            get => y;
            set => y = value;
        }
        
        public Point() : this(0,0) {}

        public Point(double x, double y)
        {
            X = x;
            Y = y;
        }

        public virtual void MoveX(double x)
        {
            this.X += x;
        }

        public virtual void MoveY(double y)
        {
            this.Y += y;
        }

        public override string ToString()
        {
            return base.ToString() + $", X = {X}, Y = {Y}";
        }
    }
}