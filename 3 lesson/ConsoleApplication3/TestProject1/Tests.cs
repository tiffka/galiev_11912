﻿using System;
using NUnit.Framework;
using SingletonExample;
using Student;

namespace TestProject1
{
    [TestFixture]
    public class Tests
    {
        [Test]
        public void Test1()
        {
            var e = new DelegateExample();
            e.Run();
        }

        [Test]
        public void Test2()
        {
            Configuration configuration = new Configuration() {Operation = 2};
            App.Instance().SetConfig(configuration);

            var executor = new Executor<int>();
            var data = 15;

            executor.Execute(data);
        }
    }
}