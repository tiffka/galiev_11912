﻿using System.Collections.Generic;

namespace Student
{
    public class StudentComparer : IComparer<Student>
    {
        public string compareField = "default";

        public StudentComparer(string compareField)
        {
            this.compareField = compareField;
        }
        public int Compare(Student p1, Student p2)
        {
            switch (compareField)
            {
                case "fio":
                    return p1.Fio.CompareTo(p2.Fio);
                case "city":
                    return p1.City.CompareTo(p2.City);
                case "birtday":
                    return p1.Birthday.CompareTo(p2.Birthday);
                case "averageScore":
                    return p1.AverageScore.CompareTo(p2.AverageScore);
                default:
                    return p1.CompareTo(p2);
            }
        }
    }
}