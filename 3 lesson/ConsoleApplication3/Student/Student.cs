﻿using System;

namespace Student
{
    public class Student: IComparable<Student>
    {
        public string City { get; set; }
        public string Fio { get; set; }
        public DateTime Birthday { get; set; }
        public decimal AverageScore { get; set; }

        public int CompareTo(Student other)
        {
            if (City.CompareTo(other.City) == 0)
            {
                if (Fio.CompareTo(other.Fio) == 0)
                {
                    if (Birthday.CompareTo(other.Birthday) == 0)
                    {
                        return AverageScore.CompareTo(other.AverageScore);
                    }
                    else return Birthday.CompareTo(other.Birthday);
                }
                else return Fio.CompareTo(other.Fio);
            }
            else return City.CompareTo(other.City);
        }

        public override string ToString()
        {
            return Fio + " from " + City + " born " + Birthday.ToString() + " with score = " + AverageScore + "\n";
        }
    }
}