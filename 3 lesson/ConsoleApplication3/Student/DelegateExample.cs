﻿using System;

namespace Student
{
    public delegate int SingleMathOperation(int x);

    public class DelegateExample
    {
        public int PowerTwo(int x)
        {
            return x * x;
        }

        public int ModSeven(int x)
        {
            return x % 7;
        }

        public double Div2(int x)
        {
            return x / 2;
        }


        public string AddPrefix(string input)
        {
            return "Mr. " + input;
        }

        public string AddSuffix(string input)
        {
            return input + " (2020)";
        }

        public void ConsoleOutput(string data)
        {
            Console.WriteLine(data);
        }

        public void Run()
        {
            SingleMathOperation dl;
            dl = PowerTwo;

            var dl1 = new SingleMathOperation(ModSeven);

            var a = 5;
            a = dl(a);
            a = dl1.Invoke(a);

            SingleMathOperation dl3 = (int x) => x - 3;
        }
    }
}