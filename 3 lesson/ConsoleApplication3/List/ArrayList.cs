﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace List
{
    public class ArrayList<T> : ICustomCollection<T> where T : IComparable<T>
    {
        T[] arr;
        int count = 0;

        public IEnumerator<T> GetEnumerator()
        {
            return new ArrayEnumerator<T>(arr);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public bool IsEmpty()
        {
            return count == 0;
        }

        public bool Contains(T data)
        {
            return IndexOf(data) != -1;
        }

        private void SetSize(int length)
        {
            var newarr = new T[length];
            for (int i = 0; i < count; i++)
            {
                newarr[i] = arr[i];
            }

            arr = newarr;
        }

        private void CheckSize()
        {
            if (count == arr.Length) SetSize(arr.Length * 2);
            if (count < arr.Length / 2)
            {
                var newLength = 1;
                while (count > newLength) newLength *= 2;
                SetSize(newLength);
            }
        }

        public void Add(T data)
        {
            if (IsEmpty())
            {
                arr = new T[1] {data};
                count = 1;
            }
            else
            {
                CheckSize();
                arr[count] = data;
                count++;
            }
        }

        public void AddRange(T[] data)
        {
            foreach (var item in data)
            {
                Add(item);
            }
        }

        public void Remove(T data)
        {
            if (IsEmpty()) return;

            for (int i = 0; i < Size(); i++)
            {
                if (arr[i].CompareTo(data) == 0)
                {
                    for (int j = i; j < Size() - 1; j++)
                    {
                        arr[j] = arr[j + 1];
                    }

                    arr[Size() - 1] = default(T);

                    count--;
                    CheckSize();
                    return;
                }
            }
        }

        public void RemoveAll(T data)
        {
            if (IsEmpty()) return;

            for (int i = 0; i < Size(); i++)
            {
                if (arr[i].CompareTo(data) == 0)
                {
                    for (int j = i; j < Size() - 1; j++)
                    {
                        arr[j] = arr[j + 1];
                    }

                    arr[Size() - 1] = default(T);
                    count--;
                    i--;
                }
            }

            CheckSize();
        }

        public void RemoveAt(int index)
        {
            if (IsEmpty()) return;
            if (index > arr.Length - 1 || index < 0) return;

            for (int j = index; j < Size() - 1; j++)
            {
                arr[j] = arr[j + 1];
            }

            arr[Size() - 1] = default(T);
            count--;

            CheckSize();
        }

        public void Clear()
        {
            count = 0;
            arr = new T[1];
        }

        public void Reverse()
        {
            var length = count;
            for (int i = 0; i < length / 2; i++)
            {
                var tmp = arr[length - 1 - i];
                arr[length - 1 - i] = arr[i];
                arr[i] = tmp;
            }
        }

        public bool EqualData(ICustomCollection<T> collection)
        {
            collection = (ArrayList<T>) collection;
            if (Size() != collection.Size()) return false;

            int i = 0;
            foreach (var item in collection)
            {
                if (item.CompareTo(arr[i]) != 0) return false;
                i++;
            }

            return true;
        }

        public void Insert(int index, T item)
        {
            CheckSize();
            if (index < 0 || index > count) throw new IndexOutOfRangeException();

            count++;
            for (int j = count; j > index; j--)
            {
                arr[j] = arr[j - 1];
            }

            arr[index] = item;
        }

        public int IndexOf(T data)
        {
            for (int i = 0; i < Size(); i++)
                if (arr[i].CompareTo(data) == 0)
                    return i;

            return -1;
        }

        public int Size()
        {
            return count;
        }

        public override string ToString()
        {
            string result = string.Empty;
            for (int i = 0; i < count; i++)
            {
                result += " " + arr[i].ToString();
            }

            return result;
        }

        public void Sort()
        {
            Array.Sort(arr);
        }

        public void Sort(IComparer<T> comparer)
        {
            Array.Sort(arr, comparer);
        }

        public void Sort(Comparison<T> comparison)
        {
            Array.Sort(arr, comparison);
        }
    }
}