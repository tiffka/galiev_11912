﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace List
{
    public class ArrayEnumerator<T> : IEnumerator<T>
    {
        private T[] arr;
        private int position = -1;

        public void Dispose()
        {
        }

        public ArrayEnumerator(T[] arr)
        {
            this.arr = arr;
        }

        public T Current
        {
            get
            {
                if (position == -1 || position >= arr.Length)
                    throw new InvalidOperationException();
                return arr[position];
            }
        }

        public bool MoveNext()
        {
            if (position < arr.Length - 1)
            {
                position++;
                return true;
            }
            else
                return false;
        }

        public void Reset()
        {
            position = -1;
        }

        object IEnumerator.Current => Current;
    }
}