﻿namespace List
{
    /// <summary>
    /// Узел двусвязного списка
    /// </summary>
    public class DoubleNode<T> {
        /// <summary>
        /// Информационное поле
        /// </summary>
        public T Data { get; set; }

        /// <summary>
        /// Ссылка на следующий узел
        /// </summary>
        public DoubleNode<T> NextNode { get; set; }

        /// <summary>
        /// Ссылка на предыдущий узел
        /// </summary>
        public DoubleNode<T> PrevNode { get; set; }

    }
}