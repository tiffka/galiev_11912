﻿using System.Collections;
using System.Collections.Generic;

namespace List
{
    public class DoubleEnumerator<T> : IEnumerator<T>
    {
        private DoubleNode<T> head;
        private DoubleNode<T> current;

        public DoubleEnumerator(DoubleNode<T> head)
        {
            this.head = head;
            this.current = new DoubleNode<T>() { Data = default(T), NextNode = head };
        }

        public bool MoveNext()
        {
            if (current.NextNode != null)
            {
                current = current.NextNode;
                return true;
            }

            return false;
        }

        public void Reset()
        {
            current = head;
        }


        public void Dispose()
        {
        }


        T IEnumerator<T>.Current => current.Data;
        object IEnumerator.Current => current.Data;
    }
}