﻿using System.Collections;
using System.Collections.Generic;

namespace List
{
    public interface ICustomCollection<T> : IEnumerable<T>
    {
        /// <summary>
        /// Получить размер
        /// </summary>
        /// <returns></returns>
        int Size();
        /// <summary>
        /// Проверяет список на наличие элементов
        /// </summary>
        /// <returns></returns>
        bool IsEmpty();
        /// <summary>
        /// Проверяет, существует ли элемент
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        bool Contains(T data);
        /// <summary>
        /// Добавить элемент
        /// </summary>
        /// <param name="data"></param>
        void Add(T data);
        /// <summary>
        /// Добавить массив элементов
        /// </summary>
        /// <param name="data"></param>
        void AddRange(T[] data);
        
        /// <summary>
        /// Удалить элемент
        /// </summary>
        /// <param name="data"></param>
        void Remove(T data);
        
        /// <summary>
        /// Удалить все элементы
        /// </summary>
        /// <param name="data"></param>
        void RemoveAll(T data);
        
        /// <summary>
        /// Удалить элемент на конкретном месте
        /// </summary>
        /// <param name="index"></param>
        void RemoveAt(int index);
        
        /// <summary>
        /// Очистить список
        /// </summary>
        void Clear();
        
        /// <summary>
        /// Перевернуть список
        /// </summary>
        void Reverse();
        
        // TODO IEnumerable
        
        /// <summary>
        /// Проверить данные на сходство
        /// </summary>
        /// <param name="collection"></param>
        /// <returns></returns>
        bool EqualData(ICustomCollection<T> collection);
        
        /// <summary>
        /// Вставить элемент в конкретное место
        /// </summary>
        /// <param name="index"></param>
        /// <param name="item"></param>
        void Insert(int index, T item);
        
        /// <summary>
        /// Получить индекс конкретного элемента
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        int IndexOf(T data);
    }
}