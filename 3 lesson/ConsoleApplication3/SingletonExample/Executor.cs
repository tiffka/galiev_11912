﻿using System;
using Student;

namespace SingletonExample
{
    public class Executor<T>
    {
        public void Execute(T input)
        {
            var config = App.Instance().getConfig();
            var e = new DelegateExample();

            switch (config.DataType)
            {
                case 1:
                    Func<int, int> func = e.PowerTwo;
                    switch (config.Operation)
                    {
                        case 1:
                            func = e.PowerTwo;
                            break;
                        case 2:
                            func = e.ModSeven;
                            break;
                    }

                    switch (config.OutputType)
                    {
                        case 1:
                            Console.WriteLine(func(Convert.ToInt32(input)));
                            break;
                    }

                    break;
                case 2:
                    Func<string, string> func2 = e.AddPrefix;
                    switch (config.Operation)
                    {
                        case 1:
                            func2 = e.AddPrefix;
                            break;
                        case 2:
                            func2 = e.AddSuffix;
                            break;
                    }


                    switch (config.OutputType)
                    {
                        case 1:
                            Console.WriteLine(func2(input.ToString()));
                            break;
                    }

                    break;
            }
        }
    }
}