﻿namespace SingletonExample
{
    public class App
    {
        private static Configuration conf;
        private static App _app;

        public Configuration getConfig()
        {
            if (conf == null) conf = new Configuration();
            return conf;
        }

        public void SetConfig(Configuration configuration)
        {
            conf = configuration;
        }

        private App()
        {
        }

        public static App Instance()
        {
            if (_app == null) _app = new App();
            return _app;
        }

        public void Execute()
        {

        }
    }
}