﻿namespace SingletonExample
{
    public class Configuration
    {
        /// <summary>
        /// 1 - int
        /// 2 - string
        /// </summary>
        public int DataType { get; set; }

        /// <summary>
        /// for int
        /// 1 - power two
        /// 2 - mod seven
        /// for string
        /// 1 -
        /// 2 -
        /// </summary>
        public int Operation { get; set; }

        /// <summary>
        /// 1 - Console
        /// 2 - File
        /// </summary>
        public int OutputType { get; set; }

        public Configuration()
        {
            DataType = 1;
            Operation = 1;
            OutputType = 1;
        }
    }
}