﻿namespace CustomList
{
    /// <summary>
    /// Узел списка
    /// </summary>
    public class Node<T> {
        /// <summary>
        /// Информационное поле
        /// </summary>
        public T Data { get; set; }

        /// <summary>
        /// Ссылка на следующий узел
        /// </summary>
        public Node<T> NextNode { get; set; }

    }
}