﻿using System;

namespace CustomList
{
    public class CustomList<T> : ICustomCollection<T> where T : IComparable<T>
    {
        private Node<T> head;

        public int Size()
        {
            int count = 0;
            Node<T> node = head;
            while (node != null)
            {
                node = node.NextNode;
                count++;
            }

            return count;
        }

        public bool IsEmpty()
        {
            return head == null;
        }

        public bool Contains(T data)
        {
            if (IsEmpty()) return false;
            Node<T> node = head;
            while (node != null)
            {
                if (node.Data.CompareTo(data) == 0) return true;
                node = node.NextNode;
            }

            return false;
        }

        public void Add(T data)
        {
            if (IsEmpty()) head = new Node<T>() {Data = data};
            else
            {
                Node<T> node = head;
                while (node.NextNode != null)
                {
                    node = node.NextNode;
                }

                node.NextNode = new Node<T>() {Data = data};
            }
        }

        public void AddRange(T[] data)
        {
            int count = data.Length;
            for (int i = 0; i < count; i++)
                Add(data[i]);
        }

        public void Remove(T data)
        {
            if (IsEmpty()) return;
            if (head.Data.CompareTo(data) == 0)
            {
                head = head.NextNode;
                return;
            }

            Node<T> prev = null;
            Node<T> node = head;

            bool found = false;

            while (node != null && !found)
            {
                if (node.Data.CompareTo(data) == 0)
                    found = true;
                else
                {
                    prev = node;
                    node = node.NextNode;
                }
            }

            if (found) prev.NextNode = node.NextNode;
        }

        public void RemoveAll(T data)
        {
            if (IsEmpty()) return;

            Node<T> prev = null;
            Node<T> node = head;

            while (node != null)
            {
                if (node.Data.CompareTo(data) == 0)
                {
                    if (prev == null)
                    {
                        head = head.NextNode;
                        node = node.NextNode;
                    }
                    else
                    {
                        prev.NextNode = node.NextNode;
                        node = node.NextNode;
                    }
                }
                else
                {
                    prev = node;
                    node = node.NextNode;
                }
            }
        }

        public void RemoveAt(int index)
        {
            int size = Size();
            if (index < 0 || index >= size) return;

            if (index == 0)
            {
                head = head.NextNode;
                return;
            }

            Node<T> prev = null;
            Node<T> node = head;

            int i = 0;
            while (i < index)
            {
                i++;
                prev = node;
                node = node.NextNode;
            }

            prev.NextNode = node.NextNode;
        }

        public void Clear()
        {
            head = null;
        }

        public void Reverse()
        {
            CustomList<T> newList = new CustomList<T>();
            while (head != null)
            {
                newList.Insert(0, head.Data);
                head = head.NextNode;
            }

            head = newList.head;
        }

        public bool EqualData(ICustomCollection<T> collection)
        {
            var list = (CustomList<T>) collection;

            var listHead = list.head;
            var node = head;

            if (list.Size() != Size()) return false;

            while (node != null)
            {
                if (node.Data.CompareTo(listHead.Data) != 0) return false;

                node = node.NextNode;
                listHead = listHead.NextNode;
            }

            return true;
        }

        public void Insert(int index, T data)
        {
            int size = Size();
            if (index < 0 || (index >= size && index > 0)) return;

            Node<T> node = head;

            if (index == 0)
            {
                if (head == null) Add(data);
                else
                {
                    head = new Node<T>() {Data = data, NextNode = head};
                }

                return;
            }

            for (int i = 0; i <= index - 1; i++)
            {
                node = node.NextNode;
            }

            node.NextNode = new Node<T>() {Data = data, NextNode = node.NextNode};
        }

        public int IndexOf(T data)
        {
            if (IsEmpty()) return -1;
            Node<T> node = head;
            int i = 0;
            while (node != null)
            {
                if (node.Data.CompareTo(data) == 0) return i;
                node = node.NextNode;
                i++;
            }

            return -1;
        }

        public void Display()
        {
            Node<T> node = head;
            while (node != null)
            {
                Console.Write(node.Data + " ");
                node = node.NextNode;
            }

            Console.WriteLine();
        }
    }
}