﻿using System;

namespace CustomList
{
    public class DoubleList<T> : ICustomCollection<T> where T : IComparable<T>
    {
        private DoubleNode<T> head;

        public int Size()
        {
            int count = 0;
            var node = head;
            while (node != null)
            {
                node = node.NextNode;
                count++;
            }

            return count;
        }

        public bool IsEmpty()
        {
            return head == null;
        }

        public bool Contains(T data)
        {
            if (IsEmpty()) return false;
            var node = head;
            while (node != null)
            {
                if (node.Data.CompareTo(data) == 0) return true;
                node = node.NextNode;
            }

            return false;
        }

        public void Add(T data)
        {
            if (IsEmpty()) head = new DoubleNode<T>() {Data = data};
            else
            {
                var node = head;
                while (node.NextNode != null)
                {
                    node = node.NextNode;
                }

                node.NextNode = new DoubleNode<T>() {Data = data, PrevNode = node};
            }
        }

        public void AddRange(T[] data)
        {
            int count = data.Length;
            for (int i = 0; i < count; i++)
                Add(data[i]);
        }

        public void Remove(T data)
        {
            if (IsEmpty()) return;
            if (head.Data.CompareTo(data) == 0)
            {
                head = head.NextNode;
                return;
            }

            DoubleNode<T> prev = null;
            var node = head;

            bool found = false;

            while (node != null && !found)
            {
                if (node.Data.CompareTo(data) == 0)
                    found = true;
                else
                {
                    prev = node;
                    node = node.NextNode;
                }
            }

            if (found)
            {
                node.PrevNode = prev;
                prev.NextNode = node.NextNode;
            }
        }

        public void RemoveAll(T data)
        {
            if (IsEmpty()) return;

            DoubleNode<T> prev = null;
            DoubleNode<T> node = head;

            while (node != null)
            {
                if (node.Data.CompareTo(data) == 0)
                {
                    if (prev == null)
                    {
                        head = head.NextNode;
                        head.PrevNode = null;
                        node = node.NextNode;
                    }
                    else
                    {
                        prev.NextNode = node.NextNode;
                        node = node.NextNode;
                        if (node != null) node.PrevNode = prev;
                    }
                }
                else
                {
                    prev = node;
                    node = node.NextNode;
                }
            }
        }

        public void RemoveAt(int index)
        {
            int size = Size();
            if (index < 0 || index >= size) return;

            if (index == 0)
            {
                head = head.NextNode;
                head.PrevNode = null;
                return;
            }

            DoubleNode<T> prev = null;
            DoubleNode<T> node = head;

            int i = 0;
            while (i < index)
            {
                i++;
                prev = node;
                node = node.NextNode;
            }

            prev.NextNode = node.NextNode;
            if (node.NextNode != null) node.NextNode.PrevNode = prev;
        }

        public void Clear()
        {
            head = null;
        }

        public void Reverse()
        {
            DoubleList<T> newList = new DoubleList<T>();
            while (head != null)
            {
                newList.Insert(0, head.Data);
                head = head.NextNode;
            }

            head = newList.head;
        }

        public bool EqualData(ICustomCollection<T> collection)
        {
            var list = (DoubleList<T>) collection;

            var listHead = list.head;
            var node = head;

            if (list.Size() != Size()) return false;

            while (node != null)
            {
                if (node.Data.CompareTo(listHead.Data) != 0) return false;

                node = node.NextNode;
                listHead = listHead.NextNode;
            }

            return true;
        }

        public void Insert(int index, T data)
        {
            int size = Size();
            if (index < 0 || (index > size && index > 0)) return;

            var node = head;

            if (index == 0)
            {
                if (head == null) Add(data);
                else
                {
                    head = new DoubleNode<T>() {Data = data, NextNode = head};
                    head.NextNode.PrevNode = head;
                }

                return;
            }

            for (int i = 0; i < index - 1; i++)
            {
                node = node.NextNode;
            }

            node.NextNode = new DoubleNode<T>() {Data = data, NextNode = node.NextNode, PrevNode = node};
        }

        public int IndexOf(T data)

        {
            if (IsEmpty()) return -1;
            var node = head;
            int i = 0;
            while (node != null)
            {
                if (node.Data.CompareTo(data) == 0) return i;
                node = node.NextNode;
                i++;
            }

            return -1;
        }

        public void Display()
        {
            var node = head;
            while (node != null)
            {
                Console.Write(node.Data + " ");
                node = node.NextNode;
            }
            Console.WriteLine();
        }
    }
}