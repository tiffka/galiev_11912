﻿using System;
using System.IO;
using CustomList;

namespace ConsoleApplication3
{
    class Runner
    {
        public static void Run()
        {
            CustomList<int> list = new CustomList<int>();

            list.Add(11);
            list.AddRange(new[] {11, 6, 12, 13, 14, 7, 15, 8, 11, 10, 25, 11});

            list.Display();

            CustomList<int> list2 = new CustomList<int>();
            list2.AddRange(new[] {11, 11, 6, 12, 13, 14, 7, 15, 8, 11, 10, 25, 11});
            Console.WriteLine("list и list2 совпадают: " + list.EqualData(list2));
            list2.Add(10);
            Console.WriteLine("list и list2 совпадают: " + list.EqualData(list2));

            list2.Clear();

            list2.AddRange(new[] {11, 11, 6, 12, 13, 14, 7, 15, 8, 11, 10, 25, 10});
            Console.WriteLine("list и list2 совпадают: " + list.EqualData(list2));

            list2.Clear();
            list2.AddRange(new[] {11, 11, 6, 12, 13, 14, 7, 15, 8, 11, 11, 10, 25, 10, 11, 11});
            Console.WriteLine("Перезапишем список list2: ");
            list2.Display();
            Console.WriteLine("Удаляем все цифры 11 из list2");
            list2.RemoveAll(11);
            list2.Display();


            Console.WriteLine("Удаляем элементы на 0, 15, 3 местах");
            list.RemoveAt(0);
            list.RemoveAt(15);
            list.RemoveAt(3);
            list.Display();


            Console.WriteLine("Вставляем элементы на 0, 4, 40, последнее места");
            list.Insert(0, 5);
            list.Insert(4, -1);
            list.Insert(40, 40);
            list.Insert(list.Size()-1, 40);
            list.Display();


            Console.WriteLine("Разворачиваем список");
            list.Reverse();
            list.Display();

            Console.WriteLine("Индекс элемента 10");
            Console.WriteLine(list.IndexOf(10));

            Console.WriteLine("Индекс элемента 100");
            Console.WriteLine(list.IndexOf(100));

            Console.WriteLine("Существование элемента 10");
            Console.WriteLine(list.Contains(10));

            Console.WriteLine("Существование элемента 100");
            Console.WriteLine(list.Contains(100));


            Console.WriteLine("Удаляем элементы на 0, 4, 40 местах");
            list.RemoveAt(0);
            list.RemoveAt(4);
            list.RemoveAt(40);
            list.Display();
        }

        public static void RunDouble()
        {
            DoubleList<int> list = new DoubleList<int>();
            list.AddRange(new[] {6, 6, 6, 12, 13, 14, 7, 6, 6, 6, 15, 8, 10, 25, 11, 6, 6});
            list.Display();

            Console.WriteLine("Удаляем числа 6, 11, 7");
            list.Remove(6);
            list.Remove(11);
            list.Remove(7);
            list.Display();

            Console.WriteLine("Удаляем все числа 6");
            list.RemoveAll(6);
            list.Display();
            

            Console.WriteLine("Удаляем элементы на позициях 0, 5, 15");
            list.RemoveAt(0);
            list.RemoveAt(5);
            list.RemoveAt(15);
            list.Display();
            
            Console.WriteLine("Вставляем элементы на позиции 0, 5, 15, последняя");
            list.Insert(0, -15);
            list.Insert(5, 150);
            list.Insert(15, 1500);
            list.Insert(list.Size(), 1500);
            list.Display();
            
            
            Console.WriteLine("Разворачиваем список");
            list.Reverse();
            list.Display();

            Console.ReadKey();
        }
    }
}