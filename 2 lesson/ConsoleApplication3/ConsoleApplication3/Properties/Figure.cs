﻿﻿/**
* @author Ilyas Galiev
* 11-912
* Task 57
*/

namespace ConsoleApplication1
{
    public class Figure
    {
        private bool visible;
        private string color;

        public Figure() : this(true, "black")
        {
        }

        public Figure(bool visible, string color)
        {
            Visible = visible;
            Color = color;
        }

        public bool Visible
        {
            get => visible;
            set => visible = value;
        }

        public string Color
        {
            get => color;
            set => color = value;
        }

        public string Visibility()
        {
            return Visible ? "visible" : "unvisible";
        }    
        public override string ToString()
        {
            return $"Visibility: {Visibility()}, color: {Color}";
        }

        public virtual void ChangeColor(string color)
        {
            this.Color = color;
        }
    }
}