﻿using System;
using List;

namespace AiSD2
{
    public class ListRunner
    {
        public static void Run()
        {
            var list0 = new CustomList<int>();
            list0.Add(1);
            list0.Add(2);
            list0.Add(3);
            list0.Add(4);

            Console.Write("list0: ");
            list0.Display();

            Console.WriteLine("item at index(1) = " + list0.ItemAt(1)?.Data);
            Console.WriteLine("item where data=3 is " + list0.ItemOf(3)?.Data);

            var list1 = new CustomList<int>();
            list1.Add(1);
            list1.Add(2);
            list1.Add(3);

            var list2 = new CustomList<int>();
            list2.Add(3);
            list2.Add(4);
            list2.Add(5);


            Console.Write("list1: ");
            list1.Display();
            Console.Write("list2: ");
            list2.Display();

            list1.Union(list2);
            Console.Write("Union of list1 & list2: ");
            list1.Display();

            Console.WriteLine();

            var list3 = new CustomList<int>();
            list3.Add(1);
            list3.Add(2);
            list3.Add(3);

            list2.Add(3);
            list2.Add(9);


            Console.Write("list3: ");
            list3.Display();

            Console.Write("list2: ");
            list2.Display();
            var list4 = list3.Intersect(list2);
            Console.Write("Intersect of list3 & list2: ");
            list4.Display();
            Console.WriteLine();

            list4.Add(5);
            list4.Add(6);
            list4.Add(7);
            var list5 = new CustomList<int>();
            list5.Add(5);
            list5.Add(6);
            list5.Add(8);
            list5.Add(9);


            Console.Write("list4: ");
            list4.Display();
            Console.Write("list5: ");
            list5.Display();

            var list6 = list4.Diff(list5);
            Console.Write("Diff between list4 & list5: ");
            list6.Display();
        }
    }
}
