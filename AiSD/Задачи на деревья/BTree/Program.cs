﻿using System;

namespace BTree
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            var tree = new BTree<int>();
            tree.Insert(6, 2);
            tree.Insert(2, 3);
            tree.Insert(3, 4);
            tree.Insert(4, 5);
            tree.Insert(9, 15);
            tree.Insert(10, 11);
            tree.Insert(8, 10);
            tree.Insert(7, 12);

            tree.Print();

            Console.WriteLine("Minimum data of leafs = " + tree.Minimal());
        }
    }
}
