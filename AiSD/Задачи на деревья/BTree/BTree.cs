﻿using System;
using System.Collections.Generic;

namespace BTree
{
    public class BTree<T>
    {
        private BTreeNode<T> root;

        public T Root()
        {
            return root != null ? root.Data : default(T);
        }

        public T Parent(int p)
        {
            if (p < 2) throw new ArgumentException("Элемент не имеет родителя");
            List<int> way = new List<int>();
            while (p > 1)
            {
                way.Insert(0, p);
                if (p % 2 == 0)
                    p = p / 2;
                else p = p / 2 + 1;
            }

            var rootCopy = root;
            for (int i = 0; i < way.Count; i++)
            {
                if (way[i] % 2 == 0)
                {
                    if (rootCopy.Left == null)
                        throw
                            new ArgumentOutOfRangeException($"Элемента на позиции {p} нет");
                    rootCopy = rootCopy.Left;
                }
                else
                {
                    if (rootCopy.Right == null)
                        throw
                            new ArgumentOutOfRangeException($"Элемента на позиции {p} нет");
                    rootCopy = rootCopy.Right;
                }
            }

            return rootCopy != null ? rootCopy.Parent.Data : default(T);
        }


        /// <summary>
        /// Поиск элемента в дереве
        /// </summary>
        public BTreeNode<T> Find(int key, BTreeNode<T> el = null)
        {
            BTreeNode<T> foundEl = null;
            if (el == null)
                el = root;
            if (el.Key == key)
                return el;
            else if (el.Left != null)
            {
                foundEl = Find(key, el.Left);
                if (foundEl != null)
                    return foundEl;
            }
            else if (el.Right != null)
            {
                foundEl = Find(key, el.Right);
                if (foundEl != null)
                    return foundEl;
            }

            return null;
        }

        /// <summary>
        /// добавление в дерево значения
        /// </summary>
        public void Insert(int key, T data)
        {
            if (root == null)
            {
                root = new BTreeNode<T>(key, data);
                return;
            }

            var rootCopy = root;
            bool searchPosition = true;
            while (searchPosition)
            {
                if (rootCopy.Key == key)
                {
                    rootCopy.Data = data;
                    return;
                }

                if (rootCopy.Key > key)
                {
                    if (rootCopy.Left == null)
                    {
                        rootCopy.Left = new BTreeNode<T>(key, data, rootCopy);
                        searchPosition = false;
                    }
                    else
                        rootCopy = rootCopy.Left;
                }
                else
                {
                    if (rootCopy.Right == null)
                    {
                        rootCopy.Right = new BTreeNode<T>(key, data, rootCopy);
                        searchPosition = false;
                    }
                    else
                        rootCopy = rootCopy.Right;
                }
            }
        }

        /// <summary>
        /// удаление узла
        /// </summary>
        public void Remove(int key)
        {
            if (root == null) throw new ArgumentException("Дерево пустое");
            var rootCopy = root;
            var search = true;
            while (search)
            {
                if (rootCopy.Key > key)
                {
                    if (rootCopy.Left == null)
                        throw new ArgumentException($"Элемент с ключом {key} отсутствует");
                    rootCopy = rootCopy.Left;
                }
                else if (rootCopy.Key < key)
                {
                    if (rootCopy.Right == null)
                        throw new ArgumentException($"Элемент с ключом {key} отсутствует");
                    rootCopy = rootCopy.Right;
                }
                else
                {
                    search = false;
                }
            }

            if (rootCopy.Right == null && rootCopy.Left == null)
            {
                var parent = rootCopy.Parent;
                if (parent == null)
                    root = null;
                else
                {
                    if (parent.Right?.Key == rootCopy.Key)
                        parent.Right = null;
                    else parent.Left = null;
                }
            }

            if (rootCopy.Right != null && rootCopy.Left == null ||
                rootCopy.Right == null && rootCopy.Left != null)
            {
                var parent = rootCopy.Parent;
                var child = rootCopy.Right != null ? rootCopy.Right : rootCopy.Left;
                if (parent == null)
                {
                    child.Parent = null;
                    root = child;
                }
                else
                {
                    child.Parent = parent;
                    if (parent.Right?.Key == rootCopy.Key)
                        parent.Right = child;
                    else parent.Left = child;
                }
            }

            if (rootCopy.Right != null && rootCopy.Left != null)
            {
                var parent = rootCopy.Parent;
                if (rootCopy.Right.Left == null)
                {
                    rootCopy.Key = rootCopy.Right.Key;
                    rootCopy.Data = rootCopy.Right.Data;
                    if (rootCopy.Right.Right != null)
                        rootCopy.Right.Right.Parent = rootCopy;
                    rootCopy.Right = rootCopy.Right.Right;
                }
                else
                {
                    var mostLeftChild = rootCopy.Right.Left;
                    while (mostLeftChild.Left != null)
                        mostLeftChild = mostLeftChild.Left;
                    rootCopy.Key = mostLeftChild.Key;
                    rootCopy.Data = mostLeftChild.Data;
                    if (mostLeftChild.Right == null)
                    {
                        mostLeftChild.Parent.Left = null;
                    }
                    else
                    {
                        mostLeftChild.Right.Parent = mostLeftChild.Parent;
                        mostLeftChild.Parent.Left = mostLeftChild.Right;
                    }
                }
            }
        }

        /// <summary>
        /// Вывод в ширину
        /// </summary>
        public void Print()
        {
            var queue = new Queue<BTreeNode<T>>();
            queue.Enqueue(root);
            PrintLevel(queue);
        }

        private void PrintLevel(Queue<BTreeNode<T>> queue)
        {
            if (queue == null || queue.Count == 0)
                return;
            var nextLevel = new Queue<BTreeNode<T>>();
            while (queue.Count != 0)
            {
                var node = queue.Dequeue();
                if (node != null)
                {
                    Console.Write(node.Data + "(key=" + node.Key + ")  ");
                    if (node.Left != null)
                        nextLevel.Enqueue(node.Left);
                    if (node.Right != null)
                        nextLevel.Enqueue(node.Right);
                }
            }

            Console.WriteLine();
            PrintLevel(nextLevel);
        }

        /// <summary>
        /// Минимальный элемент среди листьев (только для integer)
        /// </summary>
        public int Minimal()
        {
            var el = root;
            var minimum = Int32.MaxValue;
            var queue = new Queue<BTreeNode<T>>();
            FillLeafs(el, queue);
            while (queue.Count != 0)
            {
                var item = queue.Dequeue();
                if (Int32.Parse(item.Data.ToString()) < minimum) minimum = Int32.Parse(item.Data.ToString());
            }

            return minimum;
        }

        private void FillLeafs(BTreeNode<T> node, Queue<BTreeNode<T>> queue)
        {
            if (node != null)
                if (node.Right == null && node.Left == null)
                    queue.Enqueue(node);
                else
                {
                    if (node.Left != null)
                        FillLeafs(node.Left, queue);
                    if (node.Right != null)
                        FillLeafs(node.Right, queue);
                }
        }
    }
}
