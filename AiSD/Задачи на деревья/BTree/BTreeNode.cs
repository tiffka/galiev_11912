﻿namespace BTree
{
    public class BTreeNode<T>
    {
        public BTreeNode(int key, T data, BTreeNode<T> parent = null)
        {
            Data = data;
            Key = key;
            Parent = parent;
        }

        public int Key { get; set; }

        public T Data { get; set; }

        public BTreeNode<T> Left { get; set; }

        public BTreeNode<T> Right { get; set; }
        public BTreeNode<T> Parent { get; set; }
    }
}
