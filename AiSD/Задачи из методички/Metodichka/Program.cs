﻿namespace Metodichka
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Z5_7.Run();
            Z7_1.Run();
            Z7_2.Run();
            Z9_5.Run();
            Z9_15.Run();
            Z13_2.Run();
            Z13_1.Run();
        }
    }
}
