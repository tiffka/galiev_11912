﻿using System;
//занятие 7, задача 1
namespace Metodichka
{
    public class Z7_1
    {
        public static void Run()
        {
            int n = Int32.Parse(Console.ReadLine());
            Console.Write(F(n));
        }

        private static double F(int n, double res = 0)
        {
            if (n == 1) return res;
            res += Math.Log(n--);
            return F(n, res);
        }
    }
}
