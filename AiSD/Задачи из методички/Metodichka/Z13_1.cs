﻿using System;

namespace Metodichka
{
    //занятие 13, задача 1
    public class Z13_1
    {
        public static void Run()
        {
            int n = Int32.Parse(Console.ReadLine());
            int k = Int32.Parse(Console.ReadLine());
            int[] way = new int[n + 1];
            int ways = 0;
            way[1] = 1;
            for (int i = 2; i <= k; i++)
            {
                way[i] = 2 * way[i - 1];
            }

            for (int i = k + 1; i <= n; i++)
            {
                for (int j = i - k; j < i; j++)
                {
                    way[i] += way[j];
                }
            }

            Console.WriteLine(way[n]);
        }
    }
}
