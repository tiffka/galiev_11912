﻿using System;

namespace Metodichka
{
    // занятие 9-10, задача 5
    public class Z9_5
    {
        public static void Run()
        {
            var x = new int[] {1, 2, 3, 4, 5, 6, 7};
            var y = new int[] {0, 2, 3, 8, 9, 10};
            var count = 0;
            int i = 0;
            int j = 0;
            while (i != x.Length && j != y.Length)
            {
                while (j != y.Length && i != x.Length && y[j].CompareTo(x[i]) < 0)
                    j++;
                
                while (j != y.Length && i != x.Length && y[j].CompareTo(x[i]) > 0)
                    i++;

                if (j != y.Length && i != x.Length && y[j].CompareTo(x[i]) == 0)
                {
                    count++;
                    i++;
                    j++;
                }
            }

            Console.WriteLine(count);
        }
    }
}
