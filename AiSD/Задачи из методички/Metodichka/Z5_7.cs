﻿using System;
using System.Collections.Generic;

// занятие 5, задача 7
namespace Metodichka
{
    public class Z5_7
    {
        public static void Run()
        {
            Console.WriteLine("Enter expression: ");
            string s = Console.ReadLine().Trim();
            string[] exp = s.Split(' ');
            var stack = new Stack<int>();

            for (int i = 0; i < exp.Length; i++)
                if (exp[i] == "+"
                    || exp[i] == "-"
                    || exp[i] == "*")
                    if (stack.Count >= 2)
                    {
                        var a = stack.Pop();
                        var b = stack.Pop();
                        stack.Push(MakeAct(b, a, exp[i]));
                    }
                    else
                        throw new ArgumentException("Expression is wrong");
                else
                    try
                    {
                        stack.Push(Int32.Parse(exp[i]));
                    }
                    catch (Exception e)
                    {
                        throw new ArgumentException("Expression is wrong");
                    }


            if (stack.Count == 1) Console.WriteLine("Result = " + stack.Pop());
            else throw new ArgumentException("Expression is wrong");
        }

        private static int MakeAct(int first, int second, string act)
        {
            if (act == "+") return first + second;
            if (act == "-") return first - second;
            if (act == "*") return first * second;

            return 0;
        }
    }
}
