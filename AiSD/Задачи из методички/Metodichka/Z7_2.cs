﻿using System;

namespace Metodichka
{
    // занятие 7, задача 2
    public class Z7_2
    {
        public static void Run()
        {
            var list = new List();
            list.Add(5);
            list.Add(3);
            list.Add(4);


            Console.WriteLine();
            Console.WriteLine("list: ");
            var node = list._head;
            while (node != null)
            {
                Console.Write(node.Data + " ");
                node = node.NextNode;
            }

            list.Reverse();
            Console.WriteLine();

            node = list._head;
            while (node != null)
            {
                Console.Write(node.Data + " ");
                node = node.NextNode;
            }
        }
    }

    public class List
    {
        public Node _head;

        public void Reverse()
        {
            var list = new List();
            var node = _head;
            while (node != null)
            {
                list.Insert(0, node.Data);
                node = node.NextNode;
            }

            _head = list._head;
        }

        public void Insert(int index, int data)
        {
            Node node = _head;

            if (index == 0)
            {
                if (_head == null) Add(data);
                else
                {
                    _head = new Node() {Data = data, NextNode = _head};
                }

                return;
            }

            for (int i = 0; i <= index - 1; i++)
            {
                node = node.NextNode;
            }

            node.NextNode = new Node() {Data = data, NextNode = node.NextNode};
        }

        public void Add(int data)
        {
            if (_head == null) _head = new Node() {Data = data};
            else
            {
                Node node = _head;
                while (node.NextNode != null)
                {
                    node = node.NextNode;
                }

                node.NextNode = new Node() {Data = data};
            }
        }
    }

    public class Node
    {
        public int Data;
        public Node NextNode;
    }
}
