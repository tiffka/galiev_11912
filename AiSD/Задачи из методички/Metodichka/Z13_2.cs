﻿using System;

namespace Metodichka
{
    //занятие 13, задача 2
    public class Z13_2
    {
        public static void Run()
        {
            var x = new int[] {1, 2, 3, 2, 2, 2, 5, 6, 5, 3, 2, 1, 2, 3, 4, 5};
            int max = 0;
            int currLenght = 1;
            for (int i = 1; i < x.Length; i++)
            {
                if (x[i] == x[i - 1])
                {
                    currLenght++;
                }
                else
                {
                    if (currLenght > max) max = currLenght;
                    currLenght = 1;
                }
            }

            if (currLenght > max) max = currLenght;
            Console.WriteLine(max);
        }
    }
}
