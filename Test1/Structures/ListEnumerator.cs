﻿using System.Collections;
using System.Collections.Generic;

namespace Structures
{
    public class ListEnumerator<T> : IEnumerator<T>
    {
        private Node<T> head;
        private Node<T> current;

        public ListEnumerator(Node<T> head)
        {
            this.head = head;
            this.current = new Node<T>() { Data = default(T), NextNode = head };
        }

        public bool MoveNext()
        {
            if (current.NextNode != null)
            {
                current = current.NextNode;
                return true;
            }

            return false;
        }

        public void Reset()
        {
            current = head;
        }


        public void Dispose()
        {
        }


        T IEnumerator<T>.Current => current.Data;
        object IEnumerator.Current => current.Data;
    }
}