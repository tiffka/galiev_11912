﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Structures
{
    public class DeqOnTwoList<T> where T : IComparable<T>
    {
        private Stack<T> rightStack;
        private Stack<T> leftStack;

        /// <summary>
        /// Добавление в конец
        /// </summary>
        public void PushBack(T item)
        {
            if (rightStack == null) rightStack = new Stack<T>();

            rightStack.Push(item);
        }
		
		public T RightStack { get;}

        /// <summary>
        /// Удаление с конца
        /// </summary>
        public T PopBack()
        {
            if (IsEmpty()) throw new IndexOutOfRangeException("Deque is empty");

            if (rightStack.IsEmpty())
            {
                while (!leftStack.IsEmpty())
                {
                    rightStack.Push(leftStack.Pop());
                }
            }

            return rightStack.Pop();
        }

        /// <summary>
        /// Добавление в начало
        /// </summary>
        public void PushFront(T item)
        {
            if (leftStack == null) leftStack = new Stack<T>();

            leftStack.Push(item);
        }

        /// <summary>
        /// Удаление с начала
        /// </summary>
        public T PopFront()
        {
            if (IsEmpty()) throw new IndexOutOfRangeException("Deque is empty");

            if (leftStack.IsEmpty())
            {
                while (!rightStack.IsEmpty())
                {
                    leftStack.Push(rightStack.Pop());
                }
            }

            return leftStack.Pop();
        }

        /// <summary>
        /// Проверка на пустоту
        /// </summary>
        public bool IsEmpty()
        {
            if (rightStack == null) rightStack = new Stack<T>();
            if (leftStack == null) leftStack = new Stack<T>();

            return leftStack.IsEmpty() && rightStack.IsEmpty();
        }

        /// <summary>
        /// Размер коллекции
        /// </summary>
        public int Size()
        {
            if (!IsEmpty())
                return rightStack.Size() + leftStack.Size();
            return 0;
        }

        public override string ToString()
        {
            if (IsEmpty()) return "";
            var tmp = new DeqOnTwoList<T>();
            var stack = new Stack<T>();

            stack = leftStack;
            Node<T> head = null;
            if (!stack.IsEmpty()) head = new Node<T>() {Data = stack.Head.Data, NextNode = stack.Head.NextNode};
            while (head != null)
            {
                tmp.PushBack(head.Data);
                head = head.NextNode;
            }

            stack = rightStack;
            stack.Reverse();
            if (!stack.IsEmpty())
                head = new Node<T>() {Data = stack.Head.Data, NextNode = stack.Head.NextNode};
            while (head != null)
            {
                tmp.PushBack(head.Data);
                head = head.NextNode;
            }
            stack.Reverse();

            StringBuilder str = new StringBuilder();
            while (!tmp.IsEmpty())
            {
                str.Append(tmp.PopFront());
            }

            return str.ToString();
        }
    }
}