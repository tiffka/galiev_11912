﻿using System;

namespace Structures
{
    public class Stack<T> where T : IComparable<T>
    {
        private List<T> list;

        public Node<T> Head
        {
            get => list.Head;
        }

        public Stack()
        {
            list = new List<T>();
        }

        public void Push(T data)
        {
            list.Insert(0, data);
        }

        public T Pop()
        {
            var data = Peek();
            list.RemoveAt(0);
            return data;
        }

        public T Peek()
        {
            if (list.IsEmpty()) throw new IndexOutOfRangeException("Stack is empty");
            var data = list.Head.Data;
            return data;
        }

        public int Size()
        {
            return list.Size();
        }

        public bool IsEmpty()
        {
            return list.IsEmpty();
        }

        public void Reverse()
        {
            list.Reverse();
        }
    }
}