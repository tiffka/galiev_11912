﻿using System;
using Structures;

namespace Test1
{
    internal class Program
    {
        public static void Main(string[] args)
        {
        }

        static string F(int sum)
        {
            if (sum == 1) return "1";
            if (sum == 2) return "1+1";
            if (sum == 5) return F(2) + "+" + F(2) + "+" + F(1);
            return "";
        }
    }
}