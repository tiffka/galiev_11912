﻿using System;
using NUnit.Framework;
using Structures;

namespace TestProject1
{
    [TestFixture]
    public class Tests
    {
        [Test]
        public void Test1()
        {
            DeqOnTwoList<int> deqOnTwoList = new DeqOnTwoList<int>();

            deqOnTwoList.PushFront(5);
            Assert.AreEqual(deqOnTwoList.PopFront(), 5);

            deqOnTwoList.PushFront(5);
            deqOnTwoList.PushFront(7);

            Assert.AreEqual(deqOnTwoList.Size(), 2);
            Assert.AreEqual(deqOnTwoList.ToString(), "75");
            Assert.AreEqual(deqOnTwoList.IsEmpty(), false);

            Assert.AreEqual(deqOnTwoList.PopBack(), 5);
            Assert.AreEqual(deqOnTwoList.PopFront(), 7);

            Assert.AreEqual(deqOnTwoList.IsEmpty(), true);
            Assert.AreEqual(deqOnTwoList.Size(), 0);


            deqOnTwoList.PushBack(5);
            deqOnTwoList.PushBack(7);
            deqOnTwoList.PushBack(11);
            deqOnTwoList.PushFront(10);
            Assert.AreEqual(deqOnTwoList.ToString(), "105711");
            Assert.AreEqual(deqOnTwoList.Size(), 4);
            Assert.AreEqual(deqOnTwoList.IsEmpty(), false);

            Assert.AreEqual(deqOnTwoList.PopFront(), 10);
            Assert.AreEqual(deqOnTwoList.ToString(), "5711");

            Assert.AreEqual(deqOnTwoList.PopBack(), 11);
            Assert.AreEqual(deqOnTwoList.ToString(), "57");

            Assert.AreEqual(deqOnTwoList.PopBack(), 7);
            Assert.AreEqual(deqOnTwoList.ToString(), "5");
            Assert.AreEqual(deqOnTwoList.PopFront(), 5);
            Assert.AreEqual(deqOnTwoList.ToString(), "");

            Assert.AreEqual(deqOnTwoList.IsEmpty(), true);

        }
    }
}