﻿namespace EventHW
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Runner.RunDropdown();
            Runner.RunStation();
        }
    }
}