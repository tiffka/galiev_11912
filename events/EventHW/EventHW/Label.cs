﻿using System;

namespace EventHW
{
    public class Label
    {
        private string[] _states = new []{"Label for 11-912", "Label for 11-911", "Label for 11-910"};
        public void Register(Dropdown dropdown)
        {
            dropdown.OnChange += Reload;
        }

        public void Reload(Dropdown dropdown)
        {
            var value = dropdown.Value;

            Console.WriteLine(_states[value]);
        }
    }
}