﻿using System;

namespace EventHW
{
    public class Grid
    {
        private string[] _states = new []{"Table for 11-912", "Table for 11-911", "Table for 11-910"};
        public void Register(Dropdown dropdown)
        {
            dropdown.OnChange += Reload;
        }

        public void Reload(Dropdown dropdown)
        {
            var value = dropdown.Value;

            Console.WriteLine(_states[value]);
        }
    }
}