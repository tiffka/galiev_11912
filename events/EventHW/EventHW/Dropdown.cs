﻿using System;
using System.Linq;

namespace EventHW
{
    public class Dropdown
    {
        private int[] _states = new[] {0, 1, 2};
        private string[] _labels = new string[] {"11-912", "11-911", "11-910"};
        public int Value { get; set; }
        public event Action<Dropdown> OnChange;

        public void Change(int newValue)
        {
            if (!_states.Contains(newValue)) return;

            Value = newValue;
            Console.WriteLine(_labels[Value]);
            if (OnChange != null)
                OnChange(this);
        }
    }
}