﻿using System;
using System.Threading;

namespace EventHW
{
    public class Station
    {
        public string Name;
        public int Temp;
        public readonly int CritTemp = 350;
        public Action<Station> OnCrit;

        public void Work()
        {
            while (true)
            {
                Console.WriteLine("Working... Temp: " + Temp);
                Temp += 20;
                if (Temp >= CritTemp && OnCrit != null)
                {
                    OnCrit(this);
                    break;
                }

                Thread.Sleep(1000);
            }
        }
    }
}