﻿using System;

namespace EventHW
{
    public class Resque
    {
        public void Register(Station station)
        {
            station.OnCrit += Extinguish;
        }

        public void Extinguish(Station station)
        {
            Console.WriteLine("Тушим: "+station.Name);
        }
    }
}