﻿namespace EventHW
{
    public class Runner
    {
        public static void RunDropdown()
        {
            var groups = new Dropdown();
            var grid = new Grid();
            var label = new Label();

            grid.Register(groups);
            label.Register(groups);

            groups.Change(1);
        }

        public static void RunStation()
        {
            var station = new Station();
            var resque = new Resque();
            resque.Register(station);

            station.Name = "электростанция";
            station.Temp = 200;
            station.Work();
        }
    }
}